import os

# Read The Docs service was changed 2024.
# https://about.readthedocs.com/blog/2024/07/addons-by-default/#how-does-it-affect-my-projects
html_baseurl = os.environ.get("READTHEDOCS_CANONICAL_URL", "")
if os.environ.get("READTHEDOCS", "") == "True":
    html_context = {
        "READTHEDOCS": True
    }

project = 'Linux FireWire subsystem'
author = 'Takashi Sakamoto'
copyright = '2023, Takashi Sakamoto'

html_theme = 'haiku'

html_static_path = [
  '_static',
]

html_css_files = [
  'chef-cat.css',
]
