===================
About the subsystem
===================

2023/03/04 Takashi Sakamoto

.. contents:: Contents
   :depth: 2
   :local:
   :backlinks: top

Overview
========

..
  This section provides the viewers the overview of subsystem.

The overview of subsystem is illustrated in below figure.

::

      ++====================================================================++
      || User space                                                         ||
      ||  +---------+  +---------+  +---------+               +---------+   ||
      ||  | appl 1  |  | appl 2  |  | appl 3  |               | appl 4  |   ||
      ||  +----+----+  +----+----+  +----+----+               +----+----+   ||
      ||       |            |            |                         |        ||
      ++=======|============|============|=========================|========++
               |            |            | System call             |
      ++=======|============|============|=======++      ++========|========++
      ||       |            |            |       ||      ||        |        ||
      ||  +----+----+  +----+----+  +----+----+  ||      ||        |        ||
      ||  | fw0     |  | fw1     |  | fw2     |  ||      ||        |        ||
      ||  |  fw0.0  |  |  fw1.0  |  |  fw2.0  |  || bind || +------+------+ ||
      ||  |         |  |  fw1.1  |  |  fw2.1 ---------------+ unit driver | ||
      ||  |         |  |         |  |  fw2.2  |  ||      || +-------------+ ||
      ||  +----+----+  +----+----+  +----+----+  ||      ||                 ||
      ||       |            |            |       ||      || Other subsystem ||
      ||  +----+------------+------------+----+  ||      ++=================++
      ||  |           firewire-core           |  ||
      ||  +-----------------+-----------------+  ||
      ||                    |                    ||
      ||            +-------+-------+            ||
      ||            | firewire-ohci |            ||
      ||            +-------+-------+            ||
      || The subsystem      |                    ||
      ++====================|====================++
                            | PCI bus
          +-----------------+-----------------+
          |      1394 OHCI host controller    |
          |           +---------+             |
          |           | node A  |             |
          |           |         |             |
          |           | unit A1 |             |
          |           +---------+             |
          +------+---------------------+------+
                 |                     | IEEE 1394 bus
            +----+----+           +----+----+
            | node B  |           | node C  |
            |         |           |         |
            | unit B1 |           | unit C1 |
            | unit B2 |           | unit C2 |
            |         |           | unit C3 |
            +---------+           +---------+

Two devices are connected to a 1394 OHCI host controller hardware, connected by PCI bus. In the
topology of IEEE 1394 bus, the controller is identified as ``node A``. The two devices are
identified as ``node B`` and ``node C`` per each. They include some logical functions identified
as ``unit``.

In the subsystem, the host controller hardware is bound to ``firewire-ohci`` PCI driver. The driver
communicates to ``firewire-core`` bus driver, which registers and maintains some instance of device
for the node (e.g. ``fw0``) and unit (e.g. ``fw0.0``) in Linux system. It adds some character
device so that user space application operates the host controller device to communicate with the
node (e.g. ``fw1``). Additionally, it binds the unit to the unit driver so that any instance in the
other subsystem operates the host controller to communicate with the node which includes the unit
(e.g. ``fw2.1``).

The purpose of subsystem is to maintain ``firewire-core`` and ``firewire-ohci`` drivers, as well as
`the interface for the character devices and unit drivers
<https://docs.kernel.org/driver-api/firewire.html>`_ so that user space application can utilize
resources in IEEE 1394 bus.

Current Status
==============

* The latest stable releases of Linux IEEE 1394 drivers are those in the latest released Linux 4.x
  kernels or later.
* The drivers in Linux 2.4 were considered stable too but have not been updated anymore for a long
  time.

  * Therefore they lack many features and compatibility updates which went into Linux 2.6.x...4.x
    releases.

IEEE Organization Unique Identifier (OUI) Assignments
=====================================================

..
  This section provides the viewers how the node of Linux system is identified in IEEE 1394 bus.

OpenMoko project registered ``0x001F11`` to
`MAC Address Block Large (MA-L) <https://standards.ieee.org/products-programs/regauth/oui/>`_
maintained by Institute of Electrical and Electronics Engineers (IEEE). MA-L is formerly known as
OUI (Organizationally Unique Identifier). This subsystem uses the number for the value of
``vendor_id`` field in configuration ROM for self node.

OpenMoko project used the number to construct media access control address (MAC address). The rest
24 bit field has the value between ``0x023900`` and ``0x0239ff``. This subsystem uses ``0x023901``
for the value of ``model_id`` field in configuration ROM for self node.

Linux kernel v3.19 or before, ``0xd00d1e`` was used for ``vendor_id`` and ``000001`` was for
``model_id``. These values are not registered to IEEE.

Release notes
=============

The following release notes only summarize more notable changes. A complete list of IEEE 1394
related changes is available via kernel.org git

Linux kernel version 4
----------------------

Linux 4.9 (11 December 2016)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* tentative release notes as per v4.9-rc4
* ``firewire-net``

  * Add missing input validation. Invalid IP-over-1394 encapsulation headers could trigger buffer
    overflows, potentially leading up to remote code execution
    (`CVE-2016-8633 <http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-8633>`_). Also fixed in
    v4.8.7 and v4.4.31.
  * Fix IP-over-1394 link fragmentation headers that were read and written incorrectly, which
    prevented fragmented reception from/ fragmented transmission to other stacks, including Mac
    OS X, Windows XP, and older Linux' ``eth1394``.  Also fixed in v4.8.7 and v4.4.31.

Linux 4.8 (2 October 2016)
^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``snd-firewire-tascam``

  * Fix possible deadlock in hwdep interface. Also fixed in v4.7.4 and v4.4.21.

* ``snd-fireworks``

  * Fix possible deadlock in hwdep interface. Also fixed in v4.7.4, v4.4.21, v4.1.34, v3.18.43.

Linux 4.7 (24 July 2016)
^^^^^^^^^^^^^^^^^^^^^^^^

* ``snd-dice``

  * Add support for M-Audio Profire 610, and perhaps for Profire 2626.

* ``snd-firewire-lib``

  * Add tracepoints to dump a part of isochronous packet data in order to support debugging of
    device quirks and of kernel-internal packet processing.  The
    `patch changelog <https://git.kernel.org/linus/0c95c1d6197f>`_ describes how to use this
    feature.  A `subsequent commit <https://git.kernel.org/linus/a9c4284bf5a9>`_ expands the dump
    format.

* ``snd-firewire-lib``, ``snd-bebob``, ``snd-firewire-tascam``, ``snd-fireworks``

  * Remove matching of SYT fields of the outbound stream to those of the inbound stream since it
    proves to be unnecessary in practice.  The same was already done in ``snd-dice`` in Linux v4.6.
    This simplifies the kernel code and allows for finer-grained tasklet scheduling.

* ``snd-firewire-lib``, ``snd-bebob``, ``snd-firewire-digi00x``, ``snd-firewire-tascam``,
  ``snd-fireworks``, ``snd-oxfw``

  * Perform sound card registration at least 2 seconds after the last IEEE 1394 bus reset.  This
    was already implemented for ``snd-dice`` in Linux v4.5 and is now adopted by the other FireWire
    audio drivers. It ensures that the bus and device firmware settle into a ready state before
    userspace initiate I/O to the device.

Linux 4.6 (15 May 2016)
^^^^^^^^^^^^^^^^^^^^^^^

* ``snd-bebob``

  * internal rework related to streaming recovery after bus reset

* ``snd-dice``

  * Change how the dependency between number of available PCM and MIDI channels versus sampling
    rate is handled.  Also affects the hw_params ioctl interface.  Userspace should set the
    desired sampling rate before starting PCM I/O.
  * Change how phase lock is ensured and how lock status is detected.
  * Remove matching of SYT fields of the outbound stream to those of the inbound stream. This
    synchronization happened when the device was configured for internal clock.  It required a
    startup delay of the outbound stream, which caused DICE II based devices to remain silent at
    the first attempt to start streaming.
  * Add support for previously unavailable higher PCM channels on certain devices with high
    channel count, notably Focusrite Saffire PRO 40, Focusrite Liquid Saffire 56, and TC Electronic
    Studio Konnekt 48.  These devices spread the PCM and MIDI channels across 2 tx + 2 rx IEEE
    1394 channels instead of just 1 tx + 1 rx IEEE 1394 channel, as most other devices do.

* ``snd-oxfw``

  * improvements to MIDI I/O

Linux 4.5 (13 March 2016)
^^^^^^^^^^^^^^^^^^^^^^^^^

* ``snd-dice``

  * Improved handling of DICE II firmware bootstrapping during device start-up.
  * Avoid time-out when PCM streaming is started.

* ``snd-firewire-tascam``

  * Add support for TASCAM FW-1804.

* ``snd-scs1x``

  * The functionality of this driver was moved to the ``snd-oxfw`` driver, and the now obsolete
    ``snd-scs1x`` module has been removed.

Linux 4.4 (10 January 2016)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-ohci``

  * Work around JMicron JMB38x initialization quirk. Affected isochronous transmission, e.g.
    audio via FFADO or ALSA.  Also fixed in v4.3.3, v4.2.8, v4.1.15, v3.18.26, v3.16.35,
    v3.14.59, v3.12.52, v3.10.95, v3.4.113, v3.2.74.

* ``snd-bebob``

  * Fix device identifier of Mackie Onyx 1220/1620/1640 FireWire I/O card.

* ``snd-dice``

  * Fix device identifier of Mackie Onyx Blackbird and Onyx-i series.

* ``snd-firewire-digi00x``

  * New driver for Digidesign audio interfaces: Digi 002 Console, Digi 002 Rack, Digi 003
    Console, Digi 003 Rack, Digi 003 Rack+. Written by Takashi Sakamoto.

* ``snd-firewire-lib``

  * Restructured and extended for wider protocol support. Data block processing is split from
    isochronous packet processing. This work was required for integration of ``snd-firewire-digi00x``
    and ``snd-firewire-tascam`` because devices which are supported by these drivers do not comply
    with IEC 61883-6. Written by Takashi Sakamoto.

* ``snd-firewire-tascam``

  * New driver for TASCAM audio interfaces: FW-1082, FW-1884. Written by Takashi Sakamoto.

* ``snd-oxfw``

  * Fix Mackie Onyx Satellite packet discontinuity when device is used in base station mode.
  * Add support for TASCAM FireOne.
  * Fix calculation of MIDI ports.
  * Fix for statically linked (i.e. non-modular) builds.

Linux 4.3 (1 November 2015)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* No functional changes to the FireWire subsystem.

Linux 4.2 (30 August 2015)
^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``snd-bebob``

  * Add support for

    * Digidesign Mbox 2 Pro
    * Behringer FCA610, FCA1616

  * Improve BeBoB v3 support (PrismSound Orpheus, Behringer UFX1604, Behringer FCA610).
  * Improved clock source detection. Add SYT-Match support.

* ``snd-firewire-lib``

  * Internal changes related to packet handling.

* ``snd-fireworks``

  * Fix packet discontinuity with newer firmwares.  Also fixed in v4.1.6 and v3.18.21.

Linux 4.1 (21 June 2015)
^^^^^^^^^^^^^^^^^^^^^^^^

* No functional changes to the FireWire subsystem.

Linux 4.0 (12 April 2015)
^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-core``

  * The vendor ID and model ID in the Configuration ROM root directory of Linux hosts was changed
    from ``0xd00d1e:0x000001`` to ``0x001f11:0x023901``. This change makes ``firewire-core`` 's
    local Configuration ROM comply with IEEE's OUI assignment rules.
  * This has been made possible by Openmoko's granting a range of IDs to the Linux IEEE 1394
    subsystem. See IEEE OUI Assignments for additional information. This change cannot have
    negative impact on any standard protocols, but developers of private protocols may need to
    adapt if they relied on these root directory entries instead of their own unit directory
    entries.
  * The change was also backmerged into v3.12.51.

* ``snd-bebob``

  * Fix initialization of some M-Audio devices with Linux on big-endian CPUs. Also fixed in
    v3.19.5 and v3.18.13.

* ``snd-firewire-lib``, ``snd-bebob``, ``snd-dice``, ``snd-fireworks``

  * Fix device reference counting and shutdown. Also fixed in v3.19.2.

Linux kernel version 3
-----------------------

Linux 3.19 (8 February 2015)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-ohci``, ``firewire-sbp2``

  * small internal changes without functional change

* ``snd-dice``

  * Large rework by Takashi Sakamoto in order to support PCM capture and MIDI I/O. (Before, only
    PCM playback was implemented.)  This adds support for a wide range of DICE based audio
    devices.

* ``snd-firewire-lib``

  * Limit MIDI transmission rate as per specification to prevent loss of messages.

* ``snd-oxfw``

  * Driver was renamed from ``snd-firewire-speakers`` to ``snd-oxfw`` in order to reflect upcoming
    support for more audio devices based on OXFW970/971 based devices.
  * Large rework by Takashi Sakamoto in order to support PCM capture and MIDI I/O. (Before, only
    PCM playback was implemented.)
  * Additional device support implemented by Takashi Sakamoto:

     * Behringer F-Control Audio 202
     * Mackie(Loud) Onyx-i series (former models)
     * Mackie(Loud) Onyx Satellite
     * Mackie(Loud) Tapco Link.Firewire
     * Mackie(Loud) d.2 pro/d.4 pro
     * Mackie(Loud) U.420/U.420d

Linux 3.18 (7 December 2014)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firedtv``

  * Fix potential overflow of operands for Conditional Access.

* ``firewire-core``

  * Prevent kernel stack leaking into ioctl arguments. Also fixed in v3.17.4, v3.14.25, v3.12.34,
    v3.10.61, v3.4.106, v3.2.65.

* ``snd-bebob``

  * Fix Terratec Phase 88 clock source detection. Also fixed in v3.17.2 and v3.16.7.
  * Fix Terratec Phase 88 Rack FW clock source detection.
  * Fix Saffire Pro clock source detection. Also fixed in v3.17.3.

Linux 3.17 (5 October 2014)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``snd-dice``

  * Fix regression at 176.4 and 192.0 kHz sample rate. Also fixed in v3.16.4.

Linux 3.16 (3 August 2014)
^^^^^^^^^^^^^^^^^^^^^^^^^^

* Two new drivers were added:

  * ``snd-fireworks``

    * New ALSA driver, implemented by Takashi Sakamoto, for FireWire audio devices which are
      based on the Echo Digital Audio Fireworks platform. The following devices are supported:

       * Echo AudioFire 12/ 8 (until 2009 July)
       * Echo AudioFire 2/ 4/ Pre8/ 8 (since 2009 July)
       * Echo Fireworks 8/ HDMI
       * Gibson Robot Interface Pack/ GoldTop
       * Mackie Onyx 400F/ 1200F

  * ``snd-bebob``

    * New ALSA driver, implemented by Takashi Sakamoto, for FireWire audio devices based on
      BridgeCo DM1000/DM1100/DM1500 chipsets with BeBoB firmware, in particular:

       * Acoustic Reality eAR Master One
       * Apogee X-FireWire card for DA/AD/DD-16X and Rosetta 200/400
       * Apogee Ensemble
       * Behringer Digital Mixer X32 series (X-UF card)
       * Behringer XENIX UFX 1204/ 1604
       * BridgeCo RDAudio1/ Audio5
       * CME Matrix K
       * Edirol FA-66/ FA-101
       * ESI Quatafire 610
       * Focusrite Saffire, Saffire LE, Saffire PRO 10 I/O, Saffire PRO 26 I/O (the older silver
         model; newer Saffire PRO models use a different chipset)
       * iCON FireXon
       * Lynx Aurora 8/ 16 with LT-FW
       * Mackie d.2 with FireWire option
       * Mackie Onyx 1220/ 1620/ 1640 with FireWire I/O card
       * M-Audio FireWire 1814, ProjectMix I/O
       * M-Audio FireWire 410/ AudioPhile/ Solo
       * M-Audio Ozonic, NRV10, ProFire LightBridge
       * Phonic Helix Board 12/18/24 FireWire MKII
       * Phonic Helix Board 12/18/24 Universal
       * PreSonus FireBox, FP10 (FirePod), Inspire 1394
       * Prism Sound Orpheus, ADA-8XR
       * Stanton FinalScratch 2 (ScratchAmp)
       * Tascam IF-FW/DM
       * TerraTec Aureon 7.1 FireWire
       * TerraTec EWS MIC2, EWS MIC8
       * TerraTec Phase 24 FW, Phase X24p FW, Phase 88 RackFW
       * Yamaha GO44, GO46

  * ``snd-firewire-lib``

    * several extensions implemented by Takashi Sakamoto in order to support the two new drivers

* Many of the mentioned audio devices are also supported by the
  `FFADO userspace drivers <http://ffado.org/>`_ (and have been for some time). The new ALSA
  kernel drivers are an alternative to (or replacement of) the sound streaming portion of FFADO,
  i.e. the part which implements PCM and MIDI I/O.  Extra userspace software like ``ffado-mixer``
  remains required for control of device features like router, mixer, clock source, port modes,
  and so on.

* Takashi Sakamoto published an extensive
  `report <https://github.com/takaswie/alsa-firewire-report/blob/master/firewire.pdf>`_ on the
  new drivers.

Linux 3.15 (8 June 2014)
^^^^^^^^^^^^^^^^^^^^^^^^

* In v3.14, a feature was added to enable remote DMA from and to memory outside the 32 bit range
  of physical addresses, which is possible at least with LSI FW643 controllers. This feature
  clashed with at least one vendor-specific device control protocol and was therefore reverted in
  v3.15 and v3.14.7.

Linux 3.14 (30 March 2014)
^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-net``

  * Fix a use-after-free bug which could occur after transmission failures.

* ``firewire-ohci``

  * The new module parameter ``remote_dma`` (default = N, enable unfiltered remote DMA = Y)
    replaces the former build-time option CONFIG_FIREWIRE_OHCI_REMOTE_DMA.  (This kernel
    configuration option was located in the ``Kernel hacking`` menu, item ``Remote debugging over
    FireWire with ``firewire-ohci``.)  It is therefore now possible to switch on RDMA at runtime
    on all kernels with ``firewire-ohci`` loaded or built-in, for example for remote debugging,
    without the need for a custom build option.

* Fix initialization failure with Agere FW323 and some other Agere/LSI controllers.  These
  failures were a regression since Linux v3.10 inclusive.  Also fixed in v3.13.7, v3.12.15,
  v3.11.10.7, v3.10.41.

Linux 3.13 (19 January 2014)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``snd-dice``

  * New ALSA driver, written by Clemens Ladisch. This is a playback-only driver for IEEE 1394
    audio interfaces which are based on the DICE chip family (DICE-II/Jr/Mini) from TC Applied
    Technologies.
  * For the time being, this driver is being bound only to devices which do not expose recording
    channels.  Drivers which support playback + capture + MIDI are available from the FFADO
    project in the form of JACK backends, implemented in userspace.
  * There are people working on extending the ``snd-dice`` ALSA driver to support capture and MIDI
    too, and to bring similar ALSA support to other FireWire audio devices.  When this is done,
    ALSA would replace FFADO's streaming related components.  Device control however, i.e. mixer
    and router controls etc., will continue to be implemented in userspace (typically by
    ``ffado-mixer`` of the FFADO project).

Linux 3.12 (3 November 2013)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-core`` and ``firewire-ohci``

  * Fix a subsystem deadlock at bus reset which was sometimes observed if CPU resources were low.
    (This fixes a regression since Linux 3.2 inclusive. The subsystem workqueue deadlocked
    between transaction completion handling and bus reset handling if the kernel's worker thread
    pool could not be increased in time.)

Linux 3.11 (2 September 2013)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* userspace API

  * Fix corrupted video capture, seen with IIDC/DCAM applications and certain buffer settings.
    (Fixes a regression since v3.4 inclusive.  The fix has also been released in kernels v3.10.5,
    v3.8.13.7, v3.5.7.19, v3.4.56.)

* kernelspace API

  * The kernel-internal programming interface between ``firewire-core`` and protocol drivers was
    slightly changed to better support drivers which need to apply device-specific parameters
    or routines. Out-of-tree developers who maintain a stable history can
    ``git pull git://git.kernel.org/pub/scm/linux/kernel/git/ieee1394/linux1394.git fw_driver-api-transition``
    to receive commit `94a87157cde9 <https://git.kernel.org/linus/94a87157cde9">`_ from which a
    seamless transition to the current programming interface is possible.

Linux 3.10 (30 June 2013)
^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-net``

  * added support for IPv6-over-1394 compliant with
    `RFC 3146 <https://tools.ietf.org/html/rfc3146>`_, implemented by YOSHIFUJI Hideaki
  * release IR DMA context at ifdown
  * fix small memory leak at shutdown

* ``firewire-ohci``

  * fix video reception on VIA VT6306 with gstreamer, MythTV, and maybe dv4l
  * fix a startup issue with Agere/LSI FW643-e2
  * fix controller removal when controller is in suspended state
  * improved error logging

Linux 3.9 (28 April 2013)
^^^^^^^^^^^^^^^^^^^^^^^^^

* The firewire subsystem underwent only minor janitorial changes in this kernel.

Linux 3.8 (18 February 2013)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-net``

  * fix reception of fragmented multicast and broadcast datagrams

* ``firewire-sbp2``

  * pass through thin provisioning related opcodes to SPC-3 devices

* ``snd-scs1x``

  * New ALSA MIDI driver for Stanton
    `SCS.1d <http://www.stantondj.com/stanton-controllers-systems/scs1d.html>`_ and
    `SCS.1m <http://www.stantondj.com/stanton-controllers-systems/scs1m.html>`_ FireWire DJ
    controllers, written by Clemens Ladisch. (While MIDI is handled by the ``snd-scs1x`` driver,
    audio I/O with SCS.1m is handled by `FFADO <http://ffado.org/>`__ userspace drivers.)

Linux 3.7 (10 December 2012)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-core``

  * cdev ioctl ABI: On 64-bit architectures, the kernel wrote 4 bytes too many to non-zero
    ``fw_cdev_get_info.bus_reset``. This could corrupt user memory in case of x86-32 userland on
    x86-64 kernel. This was fixed also in kernel 3.6.3, 3.5.7.1, 3.4.15, 3.2.32, 3.0.47.
  * seed the random number generator with GUIDs of FireWire nodes
  * internal optimization concerning address handlers

* ``firewire-ohci``

  * more complete TI TSB41BA3D support: properly read the local selfID0.i bit

* ``sbp-target``

  * minor fixes

Linux 3.6 (30 September 2012)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-core``, ``firewire-ohci``

  * various small fixes and optimizations

* sysfs ABI

  * new attribute ``/sys/bus/firewire/devices/fw[0-9]+/is_local`` to tell local and remote nodes
    apart

Linux 3.5 (21 July 2012)
^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-core``

  * Fix mismatch between DMA mapping direction and DMA synchronization direction of isochronous
    reception buffers of userspace drivers if vma-mapped for R/W access. Depending on IOMMU
    properties of the hardware, this bug could have caused e.g. failures to capture video through
    ``libdc1394``.
  * more consistent retry stategy in device discovery/ rediscovery, and improved failure diagnostics

* ``sbp-target``

  * New driver: Adds SCSI target functionality over FireWire. This enables a Linux node to act as
    SBP-2/ SCSI device to other nodes on the FireWire bus, for example as hard disk — similar to
    FireWire Target Disk mode of the firmware of many Apple computers. Any SCSI command set
    supported by the Linux target core, multiple logical units on the target node, and concurrent
    login by initiators are supported by ``sbp-target``.
  * In the kernel configuration menu, the ``sbp-target`` option resides under
    ``Device Drivers/ Generic Target Core Mod (TCM) and ConfigFS Infrastructure/ FireWire SBP-2
    fabric module``.

Linux 3.4 (20 May 2012)
^^^^^^^^^^^^^^^^^^^^^^^

* ``<linux/firewire-cdev.h>`` API

  * Added ``FW_CDEV_IOC_FLUSH_ISO`` ioctl which lets an application get any currently completed
    isochronous packets reported. A corresponding ``libraw1394`` update to use this ioctl in
    ``raw1394_iso_recv_flush()`` has been released in libraw1394 2.0.9.

* ``firewire-core``, ``firewire-net``, ``firewire-ohci``, ``firewire-sbp2``

  * All messages to the kernel log are now consistently prefixed by driver name and device name
    or number.

* ``firewire-ohci``

  * Fix premature completion of multichannel isochronous reception DMA. Also fixed in kernel
    3.3.1, 3.2.14, 3.0.27.
  * Fix missing completion notification in isochronous I/O in case of big intervals between
    interrupt packets or with big packet headers. — Alas this introduced a regression for
    libdc1394 and FlyCap2, causing corrupted video, bogus framerates etc. with certain buffer
    settings or frame sizes. This regression has been fixed in kernels 3.11, 3.10.5, 3.8.13.7,
    3.5.7.19, 3.4.56.

* ``firewire-sbp2``

  * If the target's unit directory contains a ``Unit_Unique_ID`` entry, use it instead of the
    node unique ID as the target's GUID in the ``ieee1394_id`` sysfs attribute and consequently
    in udev's ``/dev/disk/by-id`` symbolic links.
  * Do not try to log into targets on the local node. I/O to such targets is not yet implemented,
    and local targets are probably meant to be accessed by remote initiators anyway.
  * Fix handling of SCSI sense data (deferred errors, Valid, Filemark, EOM, ILI).

Linux 3.3 (18 March 2012)
^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-ohci``

  * Add workaround for the 1394 part of Creative SoundBlaster Audigy in order to fix transaction
    failures. Also fixed in kernel 3.2.6 and 3.0.21.
  * Work around random malfunctions of Ricoh PCI Express 1394 controllers by disabling MSI
    (message signaled interrupts). Also fixed in kernel 3.2.6 and 3.0.21.

* Loosely related to FireWire (but also USB, SATA, and other interconnects): Some kernel crashes
  at hot unplug of storage devices were fixed; e.g. an infamous bug with card readers has been
  addressed.

Linux 3.2 (4 January 2012)
^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-net``

  * Increase throughput by use of unified transactions for incoming datagrams.

* ``firewire-ohci``

  * Add support for TI TSB41BA3D as local PHY. This PHY is found on special 1394b-400 cards.
  * Fix bus topology recognition in presence of a cycle master node with a wrong gap count. The
    issue was observed when switching off a bus-powered DesktopKonnekt6 audio device.
  * Properly synchronize isochronous I/O buffers on non-coherent architectures. Architectures
    with cache-coherent DMA like x86 or PPC were not affected.

Linux 3.1 (24 October 2011)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Documentation

  * Added a brief introduction into `the character device file ABI <http://git.kernel.org/?p=linux/kernel/git/torvalds/linux.git;a=blob;f=Documentation/ABI/stable/firewire-cdev;h=16d030827368b2c49cbbe396588635dfa69d6c08;hb=322a8b034003c0d46d39af85bf24fee27b902f48>`_.
    Inline documentation in the API header file was improved.

  * Added a reference documentation of `the sysfs ABI <http://git.kernel.org/?p=linux/kernel/git/torvalds/linux.git;a=blob;f=Documentation/ABI/stable/sysfs-bus-firewire;h=3d484e5dc846ed9b7625c83d948322f825c323df;hb=322a8b034003c0d46d39af85bf24fee27b902f48>`_).

* ``<linux/firewire-cdev.h>`` API

  * Unknown ioctls set errno to ``ENOTTY`` now instead of ``EINVAL``. ``FW_CDEV_EVENT_BUS_RESET``
    events are now guaranteed not to be generated before ``FW_CDEV_IOC_GET_INFO`` was used to set
    the bus reset closure. Both issues are also fixed in Linux 3.0.1 and 2.6.35.14.
  * The first ``FW_CDEV_IOC_GET_INFO`` ioctl is clarified to start reception of bus reset events;
    earlier bus reset events are discarded. This ensures events with well-defined closure value
    under all circumstances. Also fixed in kernel 3.0.1 and 2.6.35.14.
  * Fixed an issue with 32 bit userland drivers on top of 64 bit kernel, resulting in
    ``FW_CDEV_IOC_GET_INFO`` failing with ``EFAULT``. Also fixed in kernel 3.0.25. ``libraw1394``
    and ``libdc1394`` were not affected by this issue.

* ``firewire-core``

  * Fix detection failures of old Panasonic and Grundig camcorders. The failure was accompanied
    by ``giving up on config rom`` log messages after ack-busy events in ``firewire-ohci`` 's debug
    log. Also fixed in kernel 3.0.25. (The fix addresses transaction failures to these camcorders
    which had more severe effect on the current driver stack than on the linux1394 drivers of
    kernel 2.6.36 and older. While basic DV capture without AV/C control works with these
    camcorders now, full operation also requires libraw1394 changes which were released in
    ``libraw1394`` 2.0.8.)

* ``firewire-ohci``

  * Continuing the optimization work that was released in kernel 3.0, CPU utilization during
    packet queueing was reduced further, benefiting all higherlevel 1394 kernel drivers and
    userland drivers.
  * Fix I/O and PM suspend with O2Micro PCIe FireWire controllers by blacklisting MSI on them.
    Also fixed in kernel 3.0.5.
  * A fix in the core kernel's memory management addresses ``firewire-ohci`` startup failures with
    kernels that were configured for a maximum number of CPUs of something else than a power of
    two. This was a regression since kernel 2.6.38. Also fixed in kernel 3.0.3.

* ``firewire-sbp2``

  * Fix panic when the module was unloaded while a management transaction was still pending,
    e.g. login. This was a regression since kernel 3.0. Also fixed in kernel 3.0.8.

Linux 3.0 (21 July 2011)
^^^^^^^^^^^^^^^^^^^^^^^^

* This release contains a few fixes and some optimizations regarding CPU utilization of the IEEE
  1394 kernel drivers, and the following more visible changes.
* ``firewire-ohci``

  * Probing of the FireWire part on Pinnacle MovieBoard is disabled for now because it used to
    lock up the kernel. Also disabled in kernel 2.6.39.4, 2.6.35.14. Perhaps we can come up with
    a proper fix longer term, perhaps not.

* ``firewire-sbp2``

  * Management of storage devices (connect, reconnect, disconnect) is now performed in parallel
    if there are two or more devices present.

* ``snd-isight``

  * New ALSA driver for the microphones in Apple iSight FireWire webcams. It exposes front and
    rear microphone as a sound card with two PCM capture channels. This supersedes an older
    out-of-the-mainline driver with the same function, called lisight, which depended on the
    removed ieee1394 driver stack.

Linux kernel version 2.6.3x
---------------------------

Linux 2.6.39 (18 May 2011)
^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-core``

  * Increase the default timeout of outbound split transactions from 100 ms to 2000 ms. This is
    mostly just relevant to some audio devices for which the FFADO drivers used to increase this
    timeout.
  * Try to probe even nodes whose self-ID packet does not have the "link active" bit set. Some
    rare devices wrongly have this bit off and were therefore not recognized.
  * Fix the ``FW_CDEV_IOC_ADD/REMOVE_DESCRIPTOR`` ioctl()s which did not update the local Config
    ROM properly since kernel 2.6.36 if called in repetition. Also fixed in Linux 2.6.38.6.

* ``snd-firewire-speakers``

  * New ALSA driver for LaCie FireWire Speakers. Also has got some experimental support for
    Griffin FireWave.

Linux 2.6.38 (14 March 2011)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-core``

  * Fix access to some Canon camcorders, notably MV5i. Also fixed in kernel 2.6.37.1, 2.6.36.4,
    and 2.6.33.8.

* ``firewire-net``

  * Longstanding performance issues are now fixed thanks to changes in ``firewire-ohci``.
  * Show carrier status to userland.
  * Refresh the ARP cache quicker after a temporary disconnection.

* ``firewire-ohci``

  * Fix reception of asynchronous packets larger than 4080 bytes.
  * Performance of asynchronous reception has been improved considerably. Busy link situations
    that were previously very common with ``firewire-net`` and with applications that required fast
    async reception are now avoided.
  * Fix restoration of the GUID after PM resume with some BIOSes.
  * Restart isochronous DMAs after PM resume if they were running at PM suspend time. Also
    needed for broadcast reception by ``firewire-net``.

* ``firedtv``

  * Fix reporting of IR remote control key presses to recent Xorg versions.

* As always, there have also been several further low-profile improvements, e.g. a few small
  changes to ``firewire-core`` and ``firewire-ohci`` which each lower CPU utilization a little
  bit or prevent potential corner case bugs.

* **Known issues:** On systems with the kernel configured for a maximum supported number of CPU
  cores which is not a power of two, ``firewire-ohci`` of kernel 2.6.38 and newer triggers a bug
  in the kernel's memory management system, causing a crash during boot
  `bug [31572] <https://bugzilla.kernel.org/show_bug.cgi?id=31572>`_. As a workaround, set
  ``CONFIG_NR_CPUS`` to 4 instead 3, 8 instead of 6, or 16 instead of 10 etc., depending on your
  needs. This bug has been fixed (i.e. the workaround is no longer needed) in Linux 3.1 and 3.0.3.

Linux 2.6.37 (4 January 2011)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-net``

  * Fix memory leaks.
  * Fix counting of transmit statistics.
  * Fix ``firewire-net`` blocking other FireWire I/O.

* ``firewire-ohci``

  * Fix crashes or memory corruption during reception of large asynchronous requests or
    responses. These bugs were for example experienced when using ``firewire-net``. Also fixed
    in 2.6.36.2, 2.6.35.10, 2.6.34.9, 2.6.33.8, and 2.6.32.27.

* ``firedtv``

  * Add support for DVB-S2 HD channels. (HD already worked on DVB-C and presumably DVB-T.)
  * Add a module parameter to adjust CA system IDs. This is a workaround for users of Content
    Access Module and applications like MythTV, in cases when manufacturer ID of a CAM and CA
    system ID differ.

* ``dv1394``, ``eth1394``, ``ieee1394``, ``ohci1394``, ``pcilynx``, ``raw1394``, ``sbp2``,
  ``video1394``

  * These drivers, a.k.a. the Linux1394 driver stack, have been removed.

    * ``firewire-ohci`` replaces ``ohci1394``.
    * ``firewire-core`` replaces ``ieee1394``, ``raw1394``, ``video1394``.
    * ``firewire-net`` replaces ``eth1394``.
    * ``firewire-sbp2`` replaces ``sbp2``.
    * There is no replacement for ``dv1394`` and ``pcilynx``.
    * The drivers ``firedtv``, ``init_ohci1394_dma``, and ``nosy`` are not affected by these
      replacements.

* **Known issues:** The rare and quirky controllers ALi M52xx, Apple UniNorth v1, NVIDIA NForce2
  are even less well supported by ``firewire-ohci`` than by ``ohci1394``. There were some ieee1394
  based out-of-the-mainline drivers. Of them, only `lisight <https://lisight.sourceforge.net/>`_,
  an audio driver for iSight webcams, seems still useful. Work is underway to reimplement it on
  top of ``firewire-core``.

Linux 2.6.36 (20 October 2010)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* This release brings several fixes and some new features for the newer firewire driver stack.
  Some of these fixes, as far as they affect ``libraw1394`` based software, also require
  ``libraw1394`` 2.0.6 (which contains corresponding updates) or later. Furthermore, an older
  special-purpose stand-alone driver has now been merged into the mainline kernel: Nosy, a packet
  sniffer for PCILynx cards.

* ``firewire-core``, interface to userspace drivers

  * Fix transaction responses if multiple cards are present.
  * Fix lock transaction responses.
  * Add new incoming request event type which carries information about source node and extended
    transaction code. This addresses deficiencies of the prior ABI for address range mapping and
    FCP which made it impossible to write target-like drivers in userspace. It also prevented
    FFADO to work with two or more audio devices together on the bus. In order to use this ABI
    fix with ``libraw1394`` based clients like e.g. FFADO, ``libraw1394`` v2.0.6 will be
    required.  (Due to be released soon.)
  * ``FW_CDEV_VERSION``, defined in ``<linux/firewire-cdev.h>``, is no longer being updated. Old
    ``libraw1394`` code misused this constant. If your code needs to handle different
    ``firewire-cdev.h`` file versions, have a look at current ``libraw1394`` code how it can be
    done. Basically, check for particular defined ioctls and event types.
  * Fix ``fw_cdev_event_bus_reset``: Implement ``bm_node_id``. Fix missing event on the local
    node's ``/dev/fw*`` after descriptor addition or removal.
  * New feature: Support multichannel isochronous reception. A corresponding update to libraw1394
    has not been implemented yet.
  * New feature: PHY packet transmission and reception. Among else this allows to implement
    VersaPHY controllers or VersaPHY devices in userspace.
  * The address range allocation ioctl now allows to allocate a CSR anywhere in a region larger
    than the CSR.
  * Add more safeguards against bogus ioctl data and against multithread client race conditions.
  * Several updates to the inline documentation in ``<linux/firewire-cdev.h>``.

* ``firewire-core``, bus management

  * Support the following CSRs on the local node: ``STATE_CLEAR/STATE_SET`` (cmstr and abdicate
    bits), ``NODE_IDS``, ``RESET_START`` (as dummy), ``SPLIT_TIMEOUT`` (useful to FFADO with BeBoB
    devices with lots of channels), ``CYCLE_TIME`` write support, ``BUS_TIME``, ``BUSY_TIMEOUT``,
    ``PRIORITY_BUDGET``, ``MAINT_UTILITY``.
  * More careful bus manager routines WRT temporary transaction failures, Cycle Master activation.
  * Bus resets that were issued by userspace clients via bus reset ioctl or indirectly by
    descriptor addition or removal (also ``firewire-net`` insertion and removal) are now better
    integrated into bus management: The gap count is kept stable, and the period between such bus
    resets is forced to be 2 seconds or more. This period is necessary for isochronous resource
    reallocation, SBP-2 reconnect, and similar procedures.

* ``firewire-ohci``

  * Enable message-signaled interrupts
    (`MSI <http://en.wikipedia.org/wiki/Message_Signaled_Interrupts>`_) on native PCIe
    controllers. This includes Agere FW643E (i.e. FW643 rev07), but not FW643 rev06 and older nor
    VIA VT6315 nor JMicron JMB38x on which we continue to use classic pin-based interrupts due to
    hardware bugs. MSIs are furthermore not present on Texas Instruments PCIe FireWire
    controllers which are PCI devices with a PCI–PCIe bridge. — MSI support was initially also
    enabled on older FW643 and VT6317 which led to regressions in kernels 2.6.36...2.6.36.2
    inclusive. This has been corrected in kernel 2.6.36.3.
  * Enable workaround for nonmonotonic Cycle Timer on Ricoh controllers. Cycle Timer is used by
    ``libdc1394`` and by FFADO for reception timestamps.

* ``firewire-sbp2``

  * Fix stalls with "Unsolicited response" with some firmwares of 4-bay RAID enclosures.
  * Fix a memory leak in error cases.

* ``nosy``

  * "New" driver: Nosy is a traffic sniffer driver for Texas Instruments PCILynx/ PCILynx-2 based
    cards. The use cases for nosy are analysis of nonstandard protocols and as an aid in
    development of drivers, applications, or firmwares.
  * Nosy was written by Kristian Høgsberg many years ago and maintained by him in a stand-alone
    source repository. Now the driver is available in the mainline kernel sources. Its userspace
    frontend, called nosy-dump, is also maintained and distributed with the kernel sources under
    tools/firewire/.
  * PCILynx/ PCILynx-2 is a FireWire controller which is not OHCI-1394 compliant. This controller
    is found for example on IOI IOI-1394TT (PCI card), Unibrain Fireboard 400 PCI Lynx-2
    (PCI card), Newer Technology FireWire 2 Go (CardBus card), and Apple Power Mac G3 blue &
    white (onboard controller).

* There is also an obscure change to the kernel's Makefiles with a side effect: When both
  ``ohci1394`` and ``firewire-ohci`` were installed but there was no modprobe blacklist
  configuration for one or both of them, then it was reportedly more likely in recent kernels that
  ``ohci1394`` was being bound to FireWire controllers. In 2.6.36, it will more likely be
  ``firewire-ohci``.

Linux 2.6.35 (1 August 2010)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* This release brings minor fixes to the new firewire driver stack.
* The old ieee1394 driver stack — i.e. ``ieee1394``, ``ohci1394``, ``pcilynx``, ``sbp2``,
  ``eth1394``, ``raw1394``, ``video1394``, ``dv1394`` — is now scheduled for removal in
  Linux 2.6.37.
* ``firewire-core``

  * Fix inaccessibility of certain Sony camcorders, caused by bus reset loops because of conflicts
    between ``firewire-core`` 's and the camcorder firmware's bus management. Fixed by checking
    for IEEE 1394a compliance of the isochronous resource manager. Also fixed in Linux 2.6.34.1,
    2.6.33.6, 2.6.32.16.
  * more robust allocation of transaction labels and time-out of transactions
  * lseek() is not useful with ``/dev/fw*``; it is now failed instead of seemingly succeeding

* ``firewire-ohci``

  * access PHY registers more safely
  * enable IEEE 1394a enhancements in the physical layer and link layer consistently (or disable
    consistently on IEEE 1394-1995 hardware)

* ``raw1394``, ``video1394``, ``dv1394``

  * lseek() is not useful with ``/dev/raw1394``, ``/dev/video1394/*``, ``/dev/dv1394/*``; it is
    now failed instead of seemingly succeeding

* **Known issues:** ``firewire-ohci`` changes in this release caused slow-down and data corruption on
  some TI TSB82AA2 based cards. This regression is fixed in kernel 2.6.36 and 2.6.35.8.

Linux 2.6.34 (16 May 2010)
^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``firewire-core``

  * fix "giving up on config rom for node id..." with some devices with bogus configuration ROM,
    notably Panasonic AG-DV2500 tape deck
  * fix Model_ID in modalias (sysfs variable and uevent)
  * more consistency checks in userspace driver interface
  * add ``FW_CDEV_IOC_GET_CYCLE_TIMER2`` ioctl for choice of local clock
  * put ``<linux/firewire-*.h>`` files under MIT license
  * remove incomplete support of ``Bus_Time`` CSR

* ``firewire-ohci``

  * Work around hardware that does not provide reliable cycle timer register access. Currently,
    this work around is active for the following known affected chips: ALi, NEC, VIA, TI
    TSB12LV22. A tool called `test_cycle_time <http://user.in-berlin.de/~s5r6/linux1394/utils/>`_
    exists which can be used to detect faulty chips that require addition to ``firewire-ohci`` 's
    internal quirks list. Reliable cycle timer access is needed for video reception timestamps
    and audio synchronization.
  * add module parameter that allows ad hoc activation of quirk fixes
  * fix detection of maximum available IR and IT DMA contexts
  * fixes for local (loopback) requests

* ``firedtv``

  * DMA mapping fixes

* ``ohci1394_earlyinit``

  * fix boot crash regression

Linux 2.6.33 (24 February 2010)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* This release finally adapts the ``firedtv`` DVB driver to be usable with the new stack
  alternatively to the old stack. It furthermore brings a few fixes for the new stack along
  with the usual assortment of less visible updates.
* Distributors who still ship the old stack (``ieee1394``, ``ohci1394``, ``raw1394``, ``sbp2``,
  ``eth1394`` and more) should now switch to the new one (``firewire-core``, ``firewire-ohci``,
  ``firewire-sbp2``, ``firewire-net``). In the first iteration, those distributors might want to
  ship the old stack also (but blacklisted) as a fallback for their users if unforeseen problems
  with the newer replacement drivers are encountered.
* The older FireWire stack contains several known problems which are not going to be fixed;
  instead, those issues are addressed by the new stack. An incomplete list of these issues is
  kept in `bugzilla <http://bugzilla.kernel.org/show_bug.cgi?id=10046>`_. We have a guide on
  migration from the older to the newer stack.
* ``firewire-core``

  * Properly support multiple concurrent FCP listeners. This is necessary if more than one
    process or driver requires continuous control over AV/C devices (e.g. camcorders, tape decks,
    audio devices, TV tuners).
  * Some small optimizations and fixes, among them improved Topology Map register handling.
  * Proper size check in the ``add_descriptor`` ioctl. Also fixed in Linux 2.6.32.8.

* ``firewire-net``

  * Fix a crash on SMP systems.

* ``firewire-ohci``

  * Always use packet-per-buffer mode for isochronous reception, not dual-buffer mode. Reasons
    to do so are different behavior of the modes under some circumstances, and hardware bugs in
    dual-buffer mode.
  * Fix possible crash with unusual isochronous reception parameters
    (`CVE-2009-4138 <http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-4138>`_). Also fixed
    in Linux 2.6.32.2, 2.6.31.9, 2.6.27.42.
  * Fix crashes or image corruption during video reception with TSB43AB23 on 64 bit systems with
    more than 2 GB RAM. Also fixed in Linux 2.6.32.8.
  * Fix potential breakup of isochronous transmission on some controllers, e.g. probably O2Micro
    controllers.

* ``firedtv``

  * Port to new firewire stack. Depending on whether the kernel is being configured for ieee1394
    (classic stack) or firewire (new stack) or both, the ``firedtv`` driver will automatically use
    the proper stack. This means that FireDTV users are no longer forced to use the old stack.

Linux 2.6.32 (2 December 2009)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* This release fixes the new FireWire drivers for operation with `FFADO <http://www.ffado.org/>`__
  (FireWire audio driver framework) and brings some other, minor updates. There are also fixes in
  ``libraw1394``  required in order to run FFADO on top of the new drivers; these fixes have been
  released in ``libraw1394`` v2.0.5.

* ``firewire-core``, ``firewire-ohci``, ``raw1394``

  * Fix a few obscure bugs which were unlikely to show up in practice.

* ``firewire-ohci``

  * Fix start-on-cycle feature of isochronous transmissions and receptions. Especially needed for
    FFADO.
  * Expose isochronous transmit cycle timestamps to ``firewire-core`` and thus to userspace drivers.
    Especially needed for FFADO.

* ``firewire-sbp2``

  * Fix potential bugs: login timeout, invalid SCSI status

* ``firedtv``

  * Fix automatic retuning of DVB-C cards after temporary loss of antenna signal.
  * Fix I/O to Conditional Access Module.
  * Improve module parameter for runtime debugging.

Linux 2.6.31 (9 September 2009)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* This release brings improved support of fine-grained access permission policies for FireWire
  application programs in userspace, IP networking with the new FireWire driver stack, and
  support for FireWire disks larger than 2 TB (this is also available in some stable kernel
  sub-releases before 2.6.31, see below).
* ``firewire-core``, ``firewire-ohci``, ``firewire-sbp2``

  * No longer marked as "experimental" in the kernel configuration menu. Distributors who
    provided the older ieee1394 driver stack so far are encouraged to build and install both
    driver stacks. Note, the new stack does not yet support studio audio devices, nor FireDTV DVB
    interfaces. (Support is currently in development.) See Juju Migration for further guidance on
    switching to the new drivers.
  * Driver source files and header files have been reorganized. This only affects kernel driver
    developers.

* ``firewire-core``

  * Added the sysfs attribute "units" to node representations. Its path is
    ``/sys/bus/firewire/devices/fw[0-9]+/units`` and it contains tuples of
    ``specifier-ID:software-version`` of all units found on a node.
  * This is used in new udev rules, released in udev v144, which selectively grant non-root users
    access to FireWire character device files of AV/C devices (camcorders, set top boxes, audio
    devices and more) and IIDC cameras (alias DCAM, industrial cameras and webcams) while other
    device types remain inaccessible to unprivileged users by default.
  * Be even more careful with ``Broadcast_Channel`` CSR accesses to avoid clashes with buggy
    firmwares.
  * Fixed rarely occurring bugs related to shutdown of isochronous DMA contexts and to
    DMA-mapping on platforms with non-cachecoherent DMA. (The latter fix introduced a regression
    into 2.6.31-rc1, fixed since 2.6.31-rc9.)

* ``firewire-net``

  * Added as new driver for IPv4 over 1394 for the new firewire stack, replacement of et1394 in
    the old stack. Both eth1394 and ``firewire-net`` are still considered experimental due to some
    remaining, though different, bugs in them.

* ``firewire-ohci``

  * Fix video reception on Ricoh R5C832.
  * Fix video reception from more than one camera simultaneously on Agere FW643.

* ``firewire-sbp2``

  * Added support for disks bigger than 2 TB. The necessary fix in our two SBP-2 initiator
    drivers has also been released in kernel 2.6.30.5 and 2.6.27.30.

    * Note, volume sizes bigger than 2 TB also require specific support in the SBP-2 target
      chipset of FireWire disk or RAID enclosures. This is true for any operating system as
      initiator, not just Linux. Check with your enclosure vendor if in doubt.

  * Fix potential crash after "status write for unknown orb" events.

* sbp2

  * Added support for disks bigger than 2 TB. See remarks at ``firewire-sbp2``.

Linux 2.6.30 (9 June 2009)
^^^^^^^^^^^^^^^^^^^^^^^^^^

* Most of the changes relate to extensions to the ``<linux/firewire-cdev.h>`` API which are
  required for advanced usage by FireWire-enabled userspace applications, especially IIDC video
  capture.  * With these updates, the newer firewire drivers are now superior to the older
  ieee1394 drivers in the fields of industrial video acquisition/ machine vision/ microscopy
  etc. — just like the new drivers have already been superior and preferred for mass storage
  (SBP-2) in the last two or three kernel releases.
* (Regarding consumer video applications, the new drivers should now be functionally on par with
  — yet more secure than — the old drivers. Alas the new drivers cannot be used with studio audio
  equipment yet. IP networking with the new drivers has become available in kernel 2.6.31.)

* ``firewire-core``

  * Optionally expose the reception timestamp of each isochronous packet, not just of interrupt
    packets. This change is accompanied by an update of the constant ``FW_CDEV_VERSION`` from 1
    to 2.
  * Add ioctls for isochronous resource management (allocation and deallocation of isochronous
    channels and bandwidth). These do not only accomplish what the corresponding libraw1394
    functions can do, they also implement optional kernel-assisted reallocation and deallocation.
    This means that client programs do not need to implement reallocation in their bus reset
    handler anymore and often can get rid of bus reset handlers entirely. Also, isochronous
    resources can be released by the kernel if the client program crashes or shuts down
    improperly. The kernel-assisted variant of resource allocation also takes care of the grace
    period which is to be observed after bus resets.
  * Add ioctl to query the maximum transmission speed to/from a given node.
  * Add ioctl for broadcast write requests. This is for example useful to control multiple
    similar cameras at once. For device security reasons, broadcast write requests are
    restricted to addresses in Units Space.
  * Add ioctl for asynchronous stream transmission. Asynchronous streams are addressed to
    channels instead of nodes (like isochronous streams are tied to channels) but transmitted
    after asynchronous bus arbitrations (like asynchronous write requests, unlike isochronous
    streams).
  * Add various consistency checks to enforce proper cdev API usage.
  * Disallow the ``FW_CDEV_IOC_ADD_DESCRIPTOR`` ioctl on any other ``/dev/fw*`` than those of
    local nodes. This is because userland will typically maintain an access policy based on unit
    directories that are present on each node, and this ioctl may therefore influence
    accessibility of local nodes' device files.
  * Implement ``Broadcast_Channel`` register support for conformance with the 1394a bus
    management specification.

* ``firedtv``

  * Implement tuning of DVB-S2 boxes. Also implemented in kernel 2.6.29.1. This fixes tuning of
    standard definition channels. Reception of high definition channels is not yet implemented.

Linux kernel version 2.6.2x
---------------------------

Linux 2.6.29 (23 March 2009)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``dv1394``

  * Fix potential problem with interrupt control on big endian CPUs.
  * Remove unnecessary messages in the kernel log if the driver is loaded but unused.

* ``ieee1394``

  * Support 1600 Mb/s and 3200 Mb/s capable hardware. Previously there was a limit to 800 Mb/s.
    (``firewire-core`` had no such limit.)

* ``ohci1394``

  * Work around "irq...: nobody cared" and interrupt storm during resume after system suspend.
  * Increase number of hardware-assisted transmission retries when responder nodes are busy.
    Fixes AV/C control failures of camcorders with dvgrab ("send oops") and kino (loss of AV/C
    control), and possibly with other devices and software as well. Also fixed in 2.6.28.5 and
    2.6.27.16.

* ``sbp2``

  * Synchronize device quirk detection and handling with the code in the sibling driver
    ``firewire-sbp2``.
  * Add workarounds for 2nd and 3rd generation iPods. 2nd generation was found to need a request
    size limitation, 3rd generation reported a wrong capacity. This resulted in unrecoverable IO
    errors right after an affected iPod was plugged in. Also fixed in 2.6.28.5 and 2.6.27.16.

* ``firedtv``

  * New driver for FireWire-attached DVB receiver boxes and cards from Digital Everywhere, known
    as FireDTV and FloppyDTV.
  * This driver currently works together with the old 1394 driver stack only (``ieee1394`` +
    ``ohci1394``); compatibility with the new stack (``firewire-core`` + ``firewire-ohci``) will
    probably follow in the next kernel release. Also, only standard definition channels are
    supported for now; HD cannot be received yet.
  * The driver works with the DVB-C, -S, -S2, -T variants of FireDTV and FloppyDTV and also
    supports their remote control and Conditional Access Modules. The driver is meant to be used
    together with any viewer software which supports Linux' DVB programming interface, just like
    PCI or USB DVB receivers.

* ``firewire-core``

  * Fix potential crash in rapid succession of bus resets.
  * Do not detach protocol drivers (``firewire-sbp2`` or userspace driver programs) if devices
    briefly disappear or seem to disappear from the bus. This happens

    * when certain bus-powered hubs are plugged in,
    * when certain devices are plugged into 6-port hubs,
    * when certain disk enclosures are switched from self-power to bus power or vice versa and
      break the daisy chain during the transition,
    * when certain hubs temporarily malfunction during high bus traffic.

  * Fix rare bugs related to module unloading and to errors during card initialization.

* ``firewire-ohci``

  * Avoid "context_stop: still active" noise in the kernel log.
  * Increase number of hardware-assisted transmission retries when responder nodes are busy.
    Fixes AV/C control failures (like in ``ohci1394``). Also fixed in 2.6.28.5 and 2.6.27.16.

* ``firewire-sbp2``

  * Fix IOMMU resource exhaustion in an error path. Also fixed in 2.6.28.5 and 2.6.27.16.
  * Add workarounds for 2nd and 3rd generation iPods (like in ``sbp2``). Also fixed in 2.6.28.5 and
    2.6.27.16.

Linux 2.6.28 (24 December 2008)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``dv1394``

  * Change locking in the mmap() handler to prevent an unlikely deadlock.

* ``raw1394``

  * Change locking in the mmap() handler to prevent an unlikely deadlock of multithreaded client
    programs. (It nonetheless is not a good idea to use the same ``raw1394`` file handle in
    multiple threads; it may be hard to get such a client right.)
  * Change locking in the write() handler to make it multithreading-safe. (But see above.)
  * Remove the `Big Kernel Lock <http://lwn.net/Articles/86859/>`_ from the ioctl() handler for
    better system performance.

* ``ieee1394``

  * Fix corruption of CSR response handler list (ARM handlers in ``raw1394`` terminology) on
    boxes with more than one FireWire controller.
  * Do not detach the sbp2 driver if devices briefly disappear or seem to disappear from the bus.
    This happens

      * when certain bus-powered hubs are plugged in,
      * when certain disk enclosures are switched from self-power to bus power or vice versa and
        break the daisy chain during the transition,
      * when the user plugs a cable out and quickly plugs it back in, e.g. to reorder a daisy
        chain (works on Mac OS X if done quickly enough),
      * when certain hubs temporarily malfunction during high bus traffic.

  * Previously, mounted filesystems on FireWire disks would have been lost at such occasions. But
    now there is a high chance that they stay accessible.
  * Fix recognition of Freecom FireWire Harddrive. (also in 2.6.27.10)

* ``sbp2``

  * Add quirk handling for some iPod mini. (also in 2.6.27.8)

* ``firewire-core``

  * Fix kernel freeze after many bus resets. (also in 2.6.27.5)
  * Fix memory leak at bus reset. (also in 2.6.27.5)
  * Fix iso transmit parameters in the character device file interface. (also in 2.6.27.5)

* ``firewire-ohci``

  * Fix IOMMU resource leak, e.g. visible as "DMA: Out of SW-IOMMU space" on EM64T boxes and
    subsequent I/O errors. (also in 2.6.27.10)
  * Fix a memory leak at module removal.

* ``firewire-sbp2``

  * Improve login and logout handling. (also in 2.6.27.5)
  * Add quirk handling for some iPod mini. (also in 2.6.27.8)

Linux 2.6.27 (9 October 2008)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``dv1394``, ``raw1394``, ``video1394``

  * Include mmap()ed isochronous data buffers in program crash dumps.

* ``ieee1394``

  * Fix regression since Linux 2.6.25: When new nodes were added to a bus with existing ones,
    driver updates were not prioritized like in previous kernels. This especially degraded SBP-2
    reconnection.
  * Fix connection loss to old nodes when certain devices were plugged into a bus with a 6-port hub.

* ``sbp2``

  * Spin disks down (i.e. switch spindle motor off) when the system is suspended or shut down, or
    when the driver is unloaded.
  * Fix disk spin-down for PL-3507 and TSB42AA9 firmwares.
  * Fix connection loss after "Failed to reconnect to sbp2 device" during bus reset series.

* ``firewire-core``

  * IEEE 1394(a) compliance fixes: Don't respond to broadcast write requests, implement
    ``Broadcast_Channel`` register. (Not yet done: Allocate broadcast channel.)
  * Fix a kernel panic during asynchronous transmission when a bus reset occurs.
  * Fix reception of block read responses on 64bit architectures.
  * Fix a kernel warning (call trace dump) in the bus reset handling.

* ``firewire-ohci``

  * Fix isochronous reception with TSB43AB22A controllers on 64bit architectures with more than 2GB memory.

* ``firewire-core`` + ``firewire-ohci``

  * The issue with dvgrab on VIA VT630x OHCI 1.0 cards has been resolved. (DV reception got stuck
    after one or a few frames, the IR DMA context could not be woken up anymore.) The actual
    kernel change which resolved this is not known.

* ``firewire-sbp2``

  * Spin-down feature and fixes as in sbp2

* Sbp2's and ``firewire-sbp2`` 's spin-down feature is not active in multiple initiator setups, i.e. when
  non-exclusive login is configured.

Linux 2.6.26 (13 July 2008)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``ieee1394``

  * Fix speed detection with some GOF repeaters.
  * The driver ``raw1394,`` ``video1394``, ``dv1394`` are not bound to struct device instances
    anymore. This change is only important for driver programmers. It is for example necessary
    for the firesat DVB driver which is currently in the works at the Linux Driver Project.

* ``ohci1394``

  * Explicitly switch on bus power after suspend/resume on some PowerMacs and PowerBooks.

* ``raw1394``

  * Change of isochronous transmit policy: If the controller skips a cycle e.g. because of
    transmit FIFO underflow, the respective packet is now requeued for transmission in the next
    cycle instead of being dropped.
  * Related change of semantics of the struct ``raw1394_iso_status.overflows`` field: upper 16
    bits contain number of cycle skips in IT (see above), lower 16 bits contain the "overflows"
    value as before.

* ``firewire-core``

  * Properly rescan devices which changed their configuration ROM. This fixes soft-power-off of
    some FireWire harddisks and similar situations in which the drivers missed that a device went
    on or off.
  * Fixes: Bus reset loop after PHY config packet transmission; gap count synchronization between
    all nodes (device detection related); kernel panic due to unexpected reception of PHY packets.
  * After a device was removed, fail ``open()``, ``ioctl()``, ``mmap()`` properly with
    ``-ENODEV``.

* ``firewire-ohci``

  * New runtime switch to enable verbose logging in dmesg:
    ``echo N > /sys/module/firewire_ohci/parameters/debug`` with N = 0: off (default), 1:
    asynchronous packet sent or received, 2: self IDs, 4: interrupts, 8: bus reset interrupt
    events, or a combination (sum) of several of these flags.
  * fixes for asynchronous transactions on Texas Instruments controllers
  * fixes to make JMicron based cards work
  * Explicitly switch on bus power after suspend/resume on some PowerMacs and PowerBooks.
  * stricter error checks in self ID reception
  * new compile-time option ``FIREWIRE_OHCI_REMOTE_DMA`` in the ``Kernel hacking`` menu of the
    kernel configuration for remote debugging, only interesting to kernel developers

* ``firewire-sbp2``

  * Fix detection of some multi-unit devices, e.g. drives in a SONY DVD jukebox.

* **Known issues:** The new firewire drivers are still unable to capture DV on VIA VT630x OHCI
  1.0 chips. Support for isochronous resource management is still missing in the ``firewire-core``
  character device API. IP over 1394 implementation equivalent to eth1394 is still not available
  for the new stack.

Linux 2.6.25 (16 April 2008)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``sbp2``

  * Raise default size limit of requests for better throughput. This brings ``sbp2`` performance
    on par with ``firewire-sbp2`` on FireWire 800 buses. Alas some older devices cause I/O errors
    on large requests, but the known broken ones are detected and handled accordingly. If you
    encounter errors with 2.6.25 which didn't happen in previous kernels, this change may be the
    cause; please report it at ``linux1394-devel`` then.
  * Fix bogus device additions if rescan-scsi-bus.sh was run. Note, this script is not necessary
    and even was detrimental with sbp2 in the 2.6 kernel series.
  * Add workaround flag for delay between login and inquiry, flag is active for DViCO FX-3A.

* ``firewire-core``

  * Fixes for ``giving up on config rom`` errors.
  * Fix ``kobject_add failed for fw* with -EEXIST``.
  * Fix kernel crash when ``firewire-ohci`` was unloaded.

* ``firewire-ohci``

  * Fix isochronous reception for IIDC cameras.
  * Fix for systems with more than 3 GB RAM.
  * Fix for systems with IOMMU.
  * Fix for big endian CPUs.
  * Fix module reloading and suspend/resume on Apple PowerMac and PowerBook.
  * Add support for the old Apple UniNorth rev.1 controller.
  * Fix kernel panic in asynchronous transmission.
  * Handle ``cycle too long`` condition.

* ``firewire-sbp2``

  * Several fixes and improvements for device recognition, probing, and reconnection.
  * Fix I/O errors which happened as soon as the PCI bus became slightly busy. Affected were all
    SBP-2 chipsets except OxSemi ones. This fix is especially required for PL-3507 based devices.
  * Fix bugs caused by rescan-scsi-bus.sh.

* ``init_ohci1394_dma``

  * This is a new standalone driver, independent of the ieee1394 and firewire driver stacks. It
    is used for kernel development, particularly remote debugging of kernel initialization. This
    driver is configured by a kernel build config option in the ``Kernel hacking`` menu and a
    kernel boot parameter. Documentation is provided in the documentation directory of the kernel
    sources. Driver author is Bernhard Kaindl.

* **Known issues:** Regression in ``ieee1394``: When new nodes are added to a bus with existing
  ones, driver updates are not prioritized like in previous kernels. This especially degrades
  SBP-2 reconnection. Fixed in Linux 2.6.27.

Linux 2.6.24 (24 January 2008)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``eth1394``

  * Fix locking in a rare error case.

* ``ieee1394``

  * Fix small memory leak after ``eth1394`` or ``raw1394`` modified the local configuration ROM.

* ``firewire-core``

  * Adopt ``read cycle timer`` ABI like in ``raw1394`` since kernel 2.6.21.

* ``firewire-ohci``

  * Fix I/O after suspend/resume cycle for protocols which need the onfiguration ROM.
  * Fix leak of DMA mapping tables on non-coherent machines.
  * Check for unlikely error cases: PCI write posting error, self ID malconfiguration.
  * Fix DV reception for a lot of controllers. Note, a few OHCI 1.0 variants of VIA VT630x still
    don't work for DV.

* ``firewire-sbp2``

  * Add support for targets with multiple logical units, e.g. JBOD multi-disk enclosures.
  * Add module load parameter "workarounds" for emergencies. This parameter works just like in
    the classic ``sbp2`` driver.
  * Fix blocking of keyboard input and other system functions during attachment of devices.

* sparc64 architecture

  * Fix kernel oops in DMA mapping, notably when loading ``firewire-ohci``. This bug was present
    since kernel 2.6.23.

* **Known issues:** The new firewire stack is still considered experimental. There are several
  bugs in isochronous reception which especially affect ``libdc1394``. These should be fixed in
  kernel 2.6.25-rc1. There are also lots of issues with ``libraw1394`` 's interface to the new drivers
  and regarding compatibility with existing applications. Also, ``firewire-sbp2`` does not yet handle
  bus resets well enough. And work on an IP over 1394 implementation equivalent to ``eth1394`` has
  not yet started.

* Linux 2.6.24.1 fixes an "INFO: possible recursive locking detected" warning which appeared in
  the kernel log e.g. when using dvgrab or firecontrol. The issue was harmless and not caused by
  the firewire drivers.

Linux 2.6.23 (9 October 2007)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``ieee1394``

  * Properly terminate sysfs attributes ``vendor_name_kv`` and ``model_name_kv``.

* ``ohci1394``

  * Fix initialization on some hardware (some Toshiba Satellite in particular) in non-modular
    builds. This was a regression since Linux 2.6.19. Also fixed in Linux 2.6.22.9.

* ``raw1394``

  * Now almost completely fixed for 32bit userland on 64bit kernels. (There remain issues in the
    rarely used address range mapping functions.)
  * The long deprecated 1st generation isochronous ABI, consisting of the request types
    ``RAW1394_REQ_ISO_LISTEN`` and ``_SEND``, have been removed. Callers of ``libraw1394`` 's
    undocumented functions ``raw1394_iso_write``, ``raw1394_start_iso_write``,
    ``raw1394_start_iso_rcv``, and ``raw1394_stop_iso_rcv`` are affected. Only ancient and
    obsolete programs used these calls which have been replaced by the more efficient rawiso and
    ``video1394`` ABIs.

* ``sbp2``

  * Fix crash in ``sbp2`` initialization on PPC64. This was a regression since Linux 2.6.22. Also
    fixed in Linux 2.6.22.2.
  * Expose the ``IEEE1394_SBP2_PHYS_DMA`` compile-time option on x86-64.

* ``firewire-core``

  * Add support for S100B...S400B and link speed slower than PHY speed.
  * Optimize bandwidth (gap count) in mixed 1394a/1394b topologies, except with 1394b repeater
    nodes present.
  * Fix memory leak. Also fixed in Linux 2.6.22.2.

* ``firewire-ohci``

  * Fix scheduling in atomic context. Also fixed in Linux 2.6.22.2.
  * iBook G3 and older Powerbooks refused to suspend. Also fixed in Linux 2.6.22.9.
  * Fix panic on module unloading while devices were attached.

* ``firewire-sbp2``

  * Fix "status write for unknown orb" followed by I/O errors.
  * Make it work on CardBus FireWire cards. Also fixed in Linux 2.6.22.2.
  * Port from the old sbp2 driver: Limit sector count for certain old chipsets. Add module load
    parameter ``exclusive_login``.

Linux 2.6.22 (8 July 2007)
^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``eth1394``

  * Do not auto-load ``eth1394`` per hotplug whenever a FireWire controller is initialized
    (usually: when ``ohci1394`` is loaded). Hotplug events (uevents) for eth1394 are now only
    generated if another IPv4-over-1394 capable node was detected on a FireWire bus.
  * From now on, those who want eth1394 have to load it manually, or simply connect another
    IPv4-over-1394 capable node (e.g. a PC with Windows XP or OS X), or add ``eth1394`` to a
    distribution-dependent list of modules to be loaded on boot, or can simulate the old
    auto-loading together with ohci1394 by the following addition to ``/etc/modprobe.conf``:
    ``install ohci1394 /sbin/modprobe eth1394; /sbin/modprobe --ignore-install ohci1394``
  * Fix silent packet loss when eth1394 was unable to obtain transaction labels from ieee1394.
    The bug was noticeable by "eth1394: No more tlabels left" kernel messages.
  * Send GASP packets at S100 if 1394b hardware is present. Fixes eth1394 for S100B...S400B
    interconnects.
  * Allow users to configure MTU bigger than 1500 if they know what they are doing.
  * Link to a parent device (instead of a virtual parent) in sysfs. This fixes a regression since
    Linux 2.6.21: Udev rules for persistent naming of network interfaces didn't work anymore. The
    fix is also available in Linux 2.6.21.5 and later.

* ``ieee1394``

  * The values of the sysfs attribute ``address`` of unit directories were wrong.

* raw1394

  * Send asynchronous streams at S100 rather than at a random speed.

* sbp2

  * Optimized DMA direction. Fixes crash on Xen.
  * New module load parameter: ``long_ieee1394_id``. If left at the default value, nothing
    changes. If set to ``y``, the format of ``/sys/bus/scsi/devices/*:*:*:*/ieee1394_id`` is
    changed from e.g. ``0001041010004beb:0:0`` to ``0001041010004beb:00042c:0000``. This longer
    form is equivalent to the concatenation of the so-called Target Port Identifier and Logical
    Unit Identifier as per SCSI Architecture Model. The main benefit is that the same format is
    also used in the new ``firewire-sbp2`` driver. (Explanation: The ``ieee1394_id`` in sysfs is
    typically used to create persistently named links in /dev/disk/by-id/. I.e. you can add
    entries like
    ``/dev/disk/by-id/ieee1394-0001041010004beb:00042c:0000-part3  /mnt/cx1  hfsplus user,noauto,noatime,force 0 0``
    to ``/etc/fstab``. With this example you can mount /mnt/cx1 without having to worry whether
    it's ``/dev/sda`` or ``sdb`` or whatever this time you plugged the FireWire disk in. This
    feature already existed in older kernels, only the format of the ID changed slightly in 2.6.22.)

* **New alternative driver stack in Linux 2.6.22 (experimental, incomplete):**

  * ``firewire-core``

    * This is a new driver, part of the new alternative FireWire driver stack known under the
      nickname ``Juju``, written by Kristian Høgsberg. Functionality-wise, ``firewire-core`` is a
      replacement for ``ieee1394``, ``raw1394``, ``video1394``, and ``dv1394``.

  * ``firewire-ohci``

    * the new driver stack's equivalent to ``ohci1394``

* ``firewire-sbp2``

  * the new driver stack's equivalent to ``sbp2``

* See Juju Migration and
  `http://marc.info/?l=linux1394-user&m=118401466928264 <http://marc.info/?l=linux1394-user&m=118401466928264>`_
  for additional notes on ``firewire-core``, ``firewire-ohci``, and ``firewire-sbp2``. More documentation on
  the new drivers will be gradually added to this Wiki.
* **Known issue:** Sbp2 crashes on G5 Powermacs. Kernels older than 2.6.22 do not contain the
  bug. Fixed in Linux 2.6.22.2.

Linux 2.6.21 (25 April 2007)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``dv1394``

  * fixed driver crash on CardBus card ejection (also fixed in Linux 2.6.20.5)

* ``ieee1394``

  * fixed resume after suspend or hibernation: the local config ROM was not restored which broke
    protocols like SBP-2 and IP over 1394
  * fixed driver crash or kernel crash if ``ieee1394`` was loaded with parameter
    ``disable_nodemgr=1`` (new bug in Linux 2.6.20, fix is also in 2.6.20.2)
  * removed the kernel configuration options to export unused symbols for potential out-of-tree
    drivers and to translate OUIs into company names in sysfs

* ``raw1394``

  * added feature to simultaneously read the cycle timer and the local system time (At the time
    of the kernel's release, the ``libraw1394`` counterpart was in ``libraw1394`` 's Subversion
    repo, pending for the next ``libraw1394`` release)

* ``video1394``

  * now works on machines with non-coherent DMA, e.g. Intel 64bit machines (also fixed in
    2.6.20.2)

* EM64T, IA64

  * fixed kernel crash related to DMA operations in a lot of drivers, among them ``video1394``
    and ``raw1394``, on Intel 64bit machines with more than ~3GB RAM (also fixed in Linux
    2.6.19.6 and 2.6.20.2)

Linux 2.6.20 (4 February 2007)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``dv1394``

  * add deprecation note, application software should migrate to ``raw1394``

* ``ieee1394``

  * set proper symlinks at ``/sys/bus/ieee1394/driver/*/module``

* ``ohci1394``

  * fix kernel oops and strange hardware faults on some PPC machines when the module was reloaded
    (fix also in 2.6.19.2)

* ``sbp2``

  * fix recognition of capabilities of some DVD-ROM/R/W (fix also in 2.6.19.3; only Linux
    2.6.19...2.6.19.2 are affected)
  * fix handling of SCSI command ``REQUEST_SENSE``

* **Developer information:** Linux 2.6.20 is the last release within which rather stable
  ``ieee1394`` APIs for kernel drivers are maintained. Starting with Linux 2.6.21, developers
  of out-of-tree kernel drivers for FireWire hardware may be faced with more or less drastic
  changes of the kernel-internal APIs. (This does not affect userspace drivers which are built
  on top of ``libraw1394``, ``libdc1394`` and so on.)
* **Known issue:** The ``ieee1394`` option ``disable_nodemgr=1`` will lead to nonfunctional
  FireWire subsystem or even kernel lockup. This regression has been fixed in 2.6.20.2. Kernels
  before 2.6.20 do not contain the bug.

Linux kernel version 2.6.1x
---------------------------

Linux 2.6.19 (29 November 2006)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``eth1394``

  * memory alignment problem fixed

* ``ieee1394``

  * recursive locking fixed
  * optimization of per-host kernel data structures; this apparently also fixed a kernel crash in
    ``hpsb_get_tlabel``

* ``ohci1394``

  * initializes before most other device drivers to aid kernel debugging with firescope

* ``raw1394``

  * kernel freeze in address range mapping functions fixed

* ``sbp2``

  * HDDs whose motor was stopped after idle time or by another host are now automatically spun up
  * IO errors caused by ``sbp2util_node_write_no_wait failed`` fixed
  * another potential deadlock during device removal fixed
  * some combinations of Initio bridges and HDD mechanisms were wrongly detected as write
    protected; fixed by using a more standards conforming command for ``sd_mod`` 's probing

* ``video1394``

  * poll() support added

* **Known issue:** Some DVD-ROM/RW were mistaken for CD-ROM. Fixed in 2.6.19.3.

Linux 2.6.18 (19 September 2006)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``ieee1394``

  * less CPU usage in DMA housekeeping
  * support for S100...S400 1394b hardware and for devices with PHY faster than link

* ``ohci1394``

  * recover from ``cycle too long`` errors

* ``sbp2``

  * spin up Maxtor OneTouch disks when necessary (also in 2.6.17.8)

* Among the rest of the changes are minor bug fixes, small changes to in-kernel API usage, and a
  small ieee1394 low-level API extension (for driver developers: two new members in
  ``struct hpsb_host`` which affect address range mapping).
* **Known issue:** If the kernel is configured for ``RW-sem debugging``, a ``recursive locking``
  warning in knodemgrd may appear in the kernel log. This is an old bug, but probably without
  practical impact. Fixed in 2.6.19.

Linux 2.6.17 (17 June 2006)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``ieee1394``

  * Removed the dormant support for devfs

* ``ohci1394``

  * PhyUpperBound lowered from ~256 TB to 1 TB on chips which support a programmable
    PhyUpperBound

* ``raw1394``

  * 64bit compatibility code for read and write access to ``/dev/raw1394`` (not yet for ioctls)

* ``sbp2``

  * Fix kernel freeze after disconnecting a device. The fix is also available in Linux 2.6.16.2.
  * Fix compatibility regression with PL-3507 since Linux 2.6.16 (``scsi_add_device failed``).
    Also available in 2.6.16.20.
  * Added a workaround for iPods (fixes ``end_request: I/O error`` at blocks near the end of the
    disk). Also available in 2.6.16.20.
  * Added module load parameter ``workarounds``. A numeric parameter which signifies a
    combination of workarounds for quirky hardware can be given. See "modinfo sbp2". The now
    redundant parameter ``force_inquiry_hack`` will be removed in a future release.
  * Disabled ``modprobe -r ohci1394`` (or pcilynx) while logged in into an SBP-2 device,
    preventing inadvertent loss of connection.

Linux 2.6.16 (19 March 2006)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* The bulk of changes to the IEEE 1394 subsystem since Linux 2.6.15 constitutes of
  behind-the-scenes cleanups, but there were also several bug fixes.
* ``amdtp``, ``cmp``

  * These drivers were removed from the kernel. Their functionality is provided by
    ``libiec61883`` now.

* ``ieee1394``

  * Bug fix: Suspend-to-disk would deadlock if the swap partition was located on a FireWire disk.
  * Reconnection to storage devices is often quicker now when additional devices get plugged in.
  * small consistency checks added to ROM parsing routines

* ``ohci1394``

  * logs number of hardware-implemented isochronous contexts when starting

* ``raw1394``

  * bug fixes for memory deallocation in config ROM manipulation routines

* ``sbp2``

  * Bug fix: Sbp2 always failed to connect to storage devices with ``login timed out`` with ALi
    and Fujitsu host adapters. This bug was believed to date back to the Mingh dynasty.
  * Bug fix: When a device was unplugged during ongoing I/O, the subsystem could fall into a
    deadlock, disabling subsequent hotplugs. (Fix is also available in Linux 2.6.15.5.)
  * Bug fix: The 36byte inquiry hack did not work in the last few Linux releases.
  * Bug fix: Resources were not freed when SCSI inquiry failed.
  * SCSI commands are no longer converted between command sets. The SCSI command set drivers,
    especially sd, are now using the proper command set.
  * Responses to inquiry commands from userspace sg I/O are now passed through as-is.

* ``scsi``/``sd`` (as far as related to ``sbp2``)

  * Bug fix: Memory corruption was encountered when FireWire disks with Initio bridge chip or
    some other bridge chips were plugged in. Could cause error messages, application crashes,
    or kernel panic. (Fix is also available in Linux 2.6.15.5.)

* ``video1394``

  * Bug fix: Wrong timestamps were generated. (bug from Linux 2.6.11)
  * Bug fix: When an ioctl failed to get a free channel, a wrong error number was returned.

* several drivers

  * numerous code cleanups without change in functionality (or so we hope)
  * When starting, ``eth1394``, ``ohci1394``, and ``sbp2`` do not log a version number anymore.
    There is no way to keep a meaningful version number without strictly centralized development
    model.
  * 1394 subsystem enabled in arch/x86_64/defconfig

* **Known issue:** Some storage devices based on Prolific PL-3507 were not accessible anymore.
  Fixed in 2.6.16.20 and 2.6.17.

Linux 2.6.15 (2 January 2006)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``ieee1394``

  * Fix recognition of some pre-1394a devices, notably LSI based SBP-2 devices. ``ieee1394`` now
    writes the broadcast channel register only to devices which actually support the register.
    Fixes also an interoperability regression of Linux 2.6.14.
  * Fix recognition of some devices after the PC was switched off and on, notably of Motorola
    DCT6200. ``ieee1394`` posts one resume packet to wake up suspended remote ports.

* ``pci``

  * Workaround for FireWire controller of several Toshiba notebooks.

* ``sbp2``

  * Fix for occasional oops during command abortion.

* ``scsi``/``sd``

  * Fix panic or oops when runing the eject command on an iPod. Fix is also available in
    Linux 2.6.14.5.

Linux 2.6.14 (27 October 2005)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* ``raw1394``

  * fix for deadlocks on SMP machines

* ``eth1394``

  * workaround for 1394b cards, required on some architectures

* ``ieee1394``

  * bus reset handling slightly changed, helps to detect new devices after a cycle master
    related reset
  * MODALIAS variable provided to hotplug userspace scripts

* ``sbp2``

  * ``serialize_io=1`` is now default, for better interoperability (add ``options sbp2
    serialize_io=0`` to ``/etc/modprobe.conf`` for optimum performance of 1394b disks)
  * fix for hangs of the ieee1394 and scsi subsystems when devices were unplugged, fix for
    ``modprobe -r sbp2`` (these fixes are also present in Linux 2.6.13.4)

Linux 2.6.12 (17 June 2005)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Linux Kernel 2.6.12 contains a major upgrade for IEEE 1394. This is arguably the first stable
  version of Linux 1394 for 2.6 (your mileage may vary). Not only does it contain many bug fixes
  resolving all known kernel warnings, it also fixes multi-LUN support for sbp2 and contains new
  sysfs entries to support udev rules for the generic setup of ``/dev`` nodes. See our new FAQ
  item for example udev rules.
