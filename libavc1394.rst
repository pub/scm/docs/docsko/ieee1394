==========
libavc1394
==========

2023/02/18 Takashi Sakamoto

.. contents:: Contents
   :depth: 2
   :local:
   :backlinks: top

Description
===========

This library provides a programming interface to the part of AV/C specification published by the
1394 Trade Assocation. AV/C stands for Audio/Video Control. Against its comprehensive name,
applications can use the library just to control the tape transport mechansim on DV camcorders. The
other type of control is not available.

Current Status
==============

* Inactive development
* Just maintained

Repository location
===================

* `<https://sourceforge.net/p/libavc1394/code/>`_

Reference manual
================

* Not available yet.

Release notes
=============

0.5.3 (21 May 2006)
-------------------

  * bugfixes
  * new ``avc1394_transaction_block2()`` function that returns the length of the response.
  * Panel subunit indirect mode constants added to header.
  * new panelctl utility based upon 6200ch and 6200cmd from MythTV developers.

0.5.2
-----

  * remained unreleased

0.5.1
-----

  * bugfixes

0.5.0
-----

  * ``libavc1394`` has had quite a bit of new functionality hanging around in its CVS for a couple
    of years. Some of this was reorganized into forthcoming ``libiec61883``. The new features
    include the ability to make simple changes to config rom images. This was a natural and
    convenient extension of the existing ``librom1394`` functionality, not as comprehensive as
    ieee1394/csr1212.  It adds bits needed to support AMDTP applications. Finally, it adds bits
    needed to make development of target mode applications easier including an example
    ``avc_vcr.c``.
