=========================
Linux FireWire subsystem
=========================

2024/04/06 Takashi Sakamoto

.. contents:: Contents
   :depth: 2
   :local:
   :backlinks: top

Introduction
============

..
  This section provides the readers the information about what to be described in the series of
  documentation so that the readers get an understanding of the subsystem by the provided
  information in the documentation. However, the readers get no learning and tasks from the
  documentation.

IEEE 1394 as known as FireWire is the technology established between the late of 1980's and the
beginning of 1990's.  The specification is firstly published in 1995, then commercialized. Many
consumer equipments were produced until around 2010, then the market was shrank and disappeared.
As of 2023, IEEE 1394 is legacy technology.

Linux kernel has a subsystem for the technology. This document includes helpful information about
the subsystem. If interested in the subsystem itself, :doc:`About the subsystem <about>` is a good
entrance. If requiring update history of the documentation,
:ref:`About documentation <about-documentation>` will a good help.

Communication channel
=====================

..
  This section provides the readers the information about the way to communicate with the users and
  developers, as well as code of conduct in the communication.

* `linux1394-devel <http://lists.sourceforge.net/mailman/listinfo/linux1394-devel>`_ is dedicated
  for upstream development
* `linux1394-user <http://lists.sourceforge.net/mailman/listinfo/linux1394-user>`_ is dedicated
  for user support

Before using the channels, please take a look at
`Code of Conduct <https://www.kernel.org/code-of-conduct.html>`_
in Linux kernel development community.

Support and expected Contribution
=================================

..
  This section provides the readers the information about contribution to the subsystem. Especially
  the items which we expected and we do not expected.

As of 2023, few developers work to use the subsystem. We appreciate any type of help for it. For
example, below items are welcome.

* Donate configuration ROM image of your device
* Provide `information about PHY hardware <phy>` of your device compliant to IEEE 1394a or later
* Distribution packaging for maintained libraries and tools
* Bug fixes for libraries
* Bug reports to kernel drivers

Due to the limited human resources you may not receive enough support in an user level, especially
applications in user space for which the original developer is already inactive. You could receive
less help for items below.

* How to use an application?
* Why my device is not available?

Provided documents
==================

..
  This section provides the readers the information about included documents.

.. toctree::
   :hidden:

   about
   phy
   migration
   specifications
   libraw1394
   libiec61883
   libavc1394
   design

* :doc:`About the subsystem <about>`

  * The overview of subsystem is available.

* :doc:`Database of PHY device IDs <phy>`

  * The database of PHY device embedded in 1394 OHCI hardware.

* :doc:`Migration from old driver stack <migration>`

  * Linux kernel 2.6.37 or before includes old generation of stack. The guide provides the way to
    migrate to the current generation of stack.

* :doc:`Specifications <specifications>`

  * The subsystem is based on specifications relevant to IEEE 1394. The document provides their list.

..
  * :doc:`Page design <design>`
  
    * The look of document is governed by the consistent design. The document provides the information
      about it.

Repositories
============

..
  This section provides the readers the information about the subsystem, libraries, utilities, and
  resources. Especially for package maintainers in Linux distribution the way to retrieve their
  package archive.

In the subsystem, kernel stack is mainly maintained. Some library, utility, resource are available
in below repositories as well.

* Kernel stack

  * `<https://git.kernel.org/pub/scm/linux/kernel/git/ieee1394/linux1394.git/>`_

  .. * Maintained by Takashi Sakamoto

* Libraries

  * `libhinawa <https://alsa-project.github.io/gobject-introspection-docs/hinawa/>`_

    * `<https://git.kernel.org/pub/scm/libs/ieee1394/libhinawa.git/>`_
    * Operate OHCI 1394 hardware for asynchronous communication with GObject Introspection support

  * `libhinoko <https://alsa-project.github.io/gobject-introspection-docs/hinoko/>`_

    * `<https://git.kernel.org/pub/scm/libs/ieee1394/libhinoko.git/>`_
    * Operate OHCI 1394 hardware for isochronous communication with GObject Introspection support

  * hinawa-rs

    * `<https://git.kernel.org/pub/scm/libs/ieee1394/hinawa-rs.git/>`_
    * Provides FFI sys crate `hinawa-sys <https://docs.rs/hinawa-sys/>`_ and
      user API crate `hinawa <https://docs.rs/hinawa/>`_ for libhinawa as Rust language bindings

  * hinoko-rs

    * `<https://git.kernel.org/pub/scm/libs/ieee1394/hinoko-rs.git/>`_
    * Provides FFI sys crate `hinoko-sys <https://docs.rs/hinoko-sys/>`_ and
      user API crate `hinoko <https://docs.rs/hinoko/>`_ for libhinoko as Rust language bindings

  * :doc:`libraw1394 <libraw1394>`

    * `<https://git.kernel.org/pub/scm/libs/ieee1394/libraw1394.git/>`_
    * The latest release is
      `v2.1.2 <https://www.kernel.org/pub/linux/libs/ieee1394/libraw1394-2.1.2.tar.gz>`_
      (`v2.1.2 signature <https://www.kernel.org/pub/linux/libs/ieee1394/libraw1394-2.1.2.tar.sign>`_)

  * :doc:`libiec61883 <libiec61883>`

    * `<https://git.kernel.org/pub/scm/libs/ieee1394/libiec61883.git/>`_
    * The latest release is
      `v1.2.0 <https://www.kernel.org/pub/linux/libs/ieee1394/libiec61883-1.2.0.tar.gz>`_
      (`v1.2.0 signature <https://www.kernel.org/pub/linux/libs/ieee1394/libraw1394-2.1.2.tar.gz>`_)

* Utilities

  * linux-firewire-utils

    * `<https://git.kernel.org/pub/scm/utils/ieee1394/linux-firewire-utils.git/>`_
    * Linux FireWire bus inspection and configuration tools.
    * Fork `<https://github.com/cladisch/linux-firewire-utils>`_ to replace ``crpp`` Python 2
      script.

* Resources

  * (T.B.D)

Maintenance schedule
====================

..
  This section provides the readers the information about schedule to maintain the subsystem for
  the next six years.

As you know, IEEE 1394 is enough legacy. Sooner or later, users would leave. This subsystem has a
time table to close the project.

* 2023, 2024, 2025, 2026

  * ``Done``: Replace the subsystem maintainer

    * Set up repositories in `<https://git.kernel.org/>`_

  * ``Done``: Refresh web site and update information
  * Get any help from Linux Foundation to place documents for specification defined by 1394 Trade
    Association

    * If no problems, upload the documents to the web site

  * Fix issues of subsystem

    * Pull requests to Linus

  * Take over the administration of communication channels
  * Adding the list archive of linux1394-devel to `lore.kernel.org <https://korg.docs.kernel.org/lore.html>`_

  * Invite repositories of external librararies (``libavc1394``, ``libdc1394``)

    * Announcement to distribution package maintainers about the upstream shift

* 2027, 2028

  * Close announcement to applications

    * `FFMPEG <https://ffmpeg.org/>`_
    * `GStreamer <https://gstreamer.freedesktop.org/>`_
    * `VLC <https://www.videolan.org/vlc/index.ja.html>`_
    * `MythTV <https://www.mythtv.org/>`_
    * `UniCap <https://unicap-imaging.org/unicap/>`_
    * `FFADO <http://ffado.org/>`_

* 2029

  * Close the project
  * Close the communication channels
  * Resign the subsystem maintainer

After closing the project, the libraries, utilities, and resources are never updated, while
software archives are available in kernel.org. The kernel stack has no maintainer, and would be
possiblely removed from Linux operating system any day.

.. _about-documentation:

About documentation
===================

..
  This section provides the readers the information about this documentation.
  - the location of repository
  - the hosting service
  - the archive of old wiki

* The source of document is hosted in
  `<https://git.kernel.org/pub/scm/docs/docsko/ieee1394.git/>`_.

  * You can see the update history as well.

* The documentation is published at `<https://readthedocs.org/>`_

  * The kernel.org subdomain URL is `<http://ieee1394.docs.kernel.org/>`_.
  * The shared domain URL is `<https://linux1394.readthedocs.io/en/latest/>`_.
  * The host system provides `hook script service <https://korg.docs.kernel.org/docs.html>`_.

* Some document is converted from `old archive <https://archive.kernel.org/oldwiki/ieee1394.wiki.kernel.org/>`_.
