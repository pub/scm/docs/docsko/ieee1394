==========
libraw1394
==========

2023/02/18 Takashi Sakamoto

.. contents:: Contents
   :depth: 2
   :local:
   :backlinks: top

Description
===========

This library provides the series of low level API for interaction with kernel implementation to
operate resources in IEEE 1394. Since version 2, The library includes abstraction layer for
previous and current generation of kernel implementation.

Current Status
==============

* Inactive development
* Just maintained

Reference manual
================

* Not available yet.

Release notes
=============

2.1.2 (3 April 2016)
--------------------

  * Fix build with some alternative C libraries and with some older build environments. No
    functional changes.

2.1.1 (28 April 2015)
---------------------

  * Fix backwards moving cycle timer in isochronous transmit. The problem has been observed with
    audio I/O on different systems.
  * Fix crash with SIGBUS on SPARC and similar architectures.
  * Fix memory leak in address range mapping response handler.
  * Fix errno reporting to applications which relied on behavior of libraw1394 1.x.
  * Fix errno checking in the dumpiso and sendiso example programs.
  * Minor documentation improvement.

2.1.0 (29 July 2012)
--------------------

  * Behavior changes of the backend to firewire-core:

    * The isochronous reception buffer is now mapped read/writable rather than read-only.  The
      raw1394 backend already maps the buffer read/writable too.  Note that writing into the buffer
      will go wrong on architectures without cache-coherent DMA if the CPU and the OHCI write
      within the same cacheline region, so use this feature with caution.
    * Decrease memory footprint of IR and IT buffers by avoiding some internal rounding up of the
      buffer size.

  * New APIs in ``<libraw1394/raw1394.h>``

    * ``raw1394_add_config_rom_descriptor()``

      * Add contents to the Configuration ROM of the local node(s).  At runtime, this API is only
        available if running on top of  ``firewire-core``.  It could probably also be implemented
        with the raw1394 kernel driver if anybody cared.

    * ``raw1394_remove_config_rom_descriptor()``

      * Counterpart to ``raw1394_add_config_rom_descriptor()``

    * ``raw1394_read_cycle_timer_and_clock()``

      * Like the existing ``raw1394_read_cycle_timer()``, but lets the caller choose between
        ``CLOCK_REALTIME`` (which the former call is always using), ``CLOCK_MONOTONIC``, or
        ``CLOCK_MONOTONIC_RAW``. For example, this enables libffado's audio streaming to run
        undisturbed by resets of ``CLOCK_REALTIME``. At runtime, this call requires
        ``firewire-core`` underneath.

    * ``raw1394_get_speed()``

      * Returns the speed code of the maximum possible transmission speed between a given node and
        the local node. This takes maximum speeds of all participating PHYs and the two links into
        account. This can be used to configure the speed of isochronous streams. At runtime, this
        call requires firewire-core underneath.

    * ``enum raw1394_iso_speed`` was extended by ``RAW1394_ISO_SPEED_800``, ``_1600``, and
      ``_3200``.

  * New APIs in ``<libraw1394/ieee1394.h>``

    * Added preprocessor constants ``L1394_SPEED_BETA``, ``L1394_SPEED_800``, ``_1600``, and
      ``_3200``.

  * testlibraw1394

    * Added unit tests of the new APIs.

2.0.9 (2 June 2012)
-------------------

  * Fix that ``raw1394_iso_recv_flush()`` did not have any effect when running on
    ``firewire-core`` alias juju kernel drivers. This fix needs kernel 3.4 or later at runtime; on
    older kernels ``raw1394_iso_recv_flush()`` will remain uneffective on ``firewire-core`` as
    before.

  * Remove build-time dependency on linux-headers. Before, if too old kernel headers were present
    on the build system, features and fixes would be silently omitted from the build. Now all
    features and fixes are built in a fully forwards and backwards compatible manner.
  * Remove ``--with-fw-dir`` configure switch which is no longer useful.
  * Janitorial fixes.

2.0.8 (19 February 2012)
------------------------

  * Performance improvement:  Configuration ROM access via ``raw1394_read()`` is dramatically
    faster and very robust now. We read the kernel's Configuration ROM cache instead of the actual
    register of the FireWire device.
  * Interoperability fix:  Internally retry ``raw1394_read/write/lock/lock64()`` after ack-busy
    response. Most notably this fixes control of some older Panasonic-based camcorders, e.g.
    Panasonic NV-DX110, Panasonic AG-EZ30, and Grundig Scenos DLC 2000.
  * Small bug fixes.

2.0.7 (8 March 2011)
--------------------

  * Fixes playback to some audio devices based on the DICE chipset.

2.0.6 (1 November 2010)
-----------------------

  * This release brings again several fixes for using libraw1394 on top of the ``firewire-core``
    kernel driver (as opposed to ``raw1394`` which is going to be removed in kernel 2.6.37).
    Notably, request reception a.k.a. address range mapping was fixed to report proper sender node
    IDs and extended tcodes to the application.
  * Note, this and some of the other updates require linux-headers 2.6.36 or later to be present
    when ``libraw1394`` is being built.  Otherwise, the fixes will not be built into the library,
    and ``libraw1394`` will behave mostly like v2.0.5 did.  (Likewise, kernel 2.6.36 is required at
    runtime for some of these updates to be in effect.  You can run brand new ``libraw1394`` on
    older kernels and vice versa, but you would miss those fixes and features.)

2.0.5 (26 December 2009)
------------------------

  * Bug fixes, in particular for isochronous transmission and `FFADO <http://www.ffado.org/>`_.

2.0.4 (30 June 2009)
--------------------

  * Build fix for the tarball release.

2.0.3 (30 June 2009)
--------------------

  * Some bug fixes, notably:

    * Build fix (Gentoo `bug 272540 <https://bugs.gentoo.org/272540>`_)
    * Fix a memory leak when running on the new firewire stack

  * Functional enhancements for the new firewire stack (no ``libraw1394`` API change involved):

    * Enable channel and bandwidth management functions even if access to the IRM's ``/dev/fw*``
      is denied.
    * Provide accurate isoch packet reception timestamps.
    * Add as yet unimplemented juju versions of broadcast requests and async streams.

  * These changes are only available with kernel-headers 2.6.30 or later at build time and kernel
    2.6.30 or later at runtime.

2.0.2 (3 February 2009)
-----------------------

  * Fixed license headers on new files introduced in v2.0.0 to be consistent with LGPL v2.1. No
    code changes since version 2.0.1.

2.0.1 (15 January 2009)
-----------------------

  * Optimized usage of ``/dev/fw*`` nodes based upon user permissions.
  * many bugfixes

2.0.0 (18 July 2008)
--------------------

  * added support for the new firewire kernel subsystem. (kernel headers 2.6.22 or better yet
    2.6.24 should be installed to compile libraw1394 with this feature)
  * removed deprecated isochronous functions.

1.3.0 (13 October 2007)
-----------------------

  * added ``raw1394_read_cycle_timer()`` as used by FFADO project.
  * added support for environment variable ``RAW1394DEV`` to override default device node
    ``/dev/raw1394``.
  * added ``--with-valgrind`` to configure script.
  * other optimizations and bugfixes - see ChangeLog in the tarball.

1.2.1 (8 April 2006)
--------------------

  * This is a maintenance release that fixes a couple of bugs and contains a slight memory handling
    optimization for isochronous I/O.

1.2.0 (19 February 2005)
------------------------

  * One more preparation for the release of ``libiec61883`` was to reorganize a couple of functions
    from ``libiec61883`` into ``libraw1394`` for better code reuse with other projects. These
    functions provide simple means to allocate and release isochronous channels and bandwidth.

1.1.0 (29 November 2004)
------------------------

  * Yes, the version number difference to the previous version seems odd, but it is correct. The
    ``.so`` version numbers were bumped a major level with no backward compatibility as well due to
    an interface change in the new generation isochronous API. The change allows one to specify a
    capture mode, and it is required by the forthcoming ``libiec61883``, our third generation
    streaming media I/O library. This release also includes updated API reference documentation.
    All comments for function documentation were moved into the main header, ``raw1394.h`` for
    convenience to programmers that like to refer to headers. Finally, two functions were added for
    the ARM feature to allow getting and setting the kernel buffer.
