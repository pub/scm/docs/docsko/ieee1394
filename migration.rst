===============================
Migration from old driver stack
===============================

2023/02/17 Takashi Sakamoto

.. contents:: Contents
   :depth: 2
   :local:
   :backlinks: top

Compatibility and stability
===========================

In Q4/2010, the new FireWire kernel driver stack (alias Juju) became feature-complete and ready to
replace the older ieee1394 stack. Here is a brief comparison to the older stack:

* Storage devices: The new stack works better.
* Video devices: The new stack generally works better or as well as the old one.
* Audio devices: Caveats apply.
* Controllers: All more or less widespread controllers are known to work. There are known issues
  with some rare controllers or rarely used software, see below.

Audio devices require kernel 2.6.32 or later, ``libraw1394`` 2.0.5 or later, and ``libffado`` 2.0.1
or later. Recommended for audio devices are kernel 2.6.36 or later, ``libraw1394`` 2.0.7 or later,
and ``libffado`` 2.1.0 or later.

Some old camcorders require kernel 3.1 or later and ``libraw1394`` 2.0.8 or later.

IP over 1394 users should use kernel 2.6.38 or later.

New stack not yet working as the old stack does
-----------------------------------------------

* Controllers

  * ALi M525x

    * Some cards with these controllers don't work yet. These controllers are comparably rare
      though. They were available as mid-range priced PCI cards (possibly also CardBus cards)
      and can be swapped for any cheap FireWire PCI card or CardBus card as a stop-gap
      solution.

  * Apple UniNorth rev.1

    * There seem to be problems with this controller, found in PowerBook G3 Pismo and G4 Titanium.
      These controllers are very rare nowadays. It is possible to use a cheap CardBus FireWire
      controller in all affected machines instead.

  * NVIDIA NForce2

    * May or may not work with the old drivers, do not yet work with the new drivers. These
      controllers are very rare. It is possible to use a cheap FireWire PCI controller in all
      affected machines instead. Note that there were reports that NForce2 did not work for some
      with the old drivers either, so this may not actually be a regression.

  * Pinnacle MovieBoard

    * ``firewire-ohci`` `hangs <http://thread.gmane.org/gmane.linux.kernel.firewire.devel/14972>`_
      in an interrupt loop at shutdown or 1panics
      `<http://thread.gmane.org/gmane.linux.kernel.firewire.devel/14982>`_ already at startup.
      These problems did apparently not happen with the older ``ohci1394`` driver, but the extent
      to which these card worked (or didn't) with ``ohci1394`` is not clear.

  * TI PCILynx TSB12LV21

    * No support for PCILynx or PCILynx2 controllers. Unlike TI OHCI-Lynx, PCILynx do not implement
      the OHCI-1394 standard and are nowadays very rare, hence it is unlikely that support for them
      will ever be written for the new driver stack. Note that their support in the old stack is
      extremely limited as well (no isochronous I/O which means no video, no audio, not even IP
      over 1394; slow and possibly very unstable SBP-2 support i.e. storage support). For all
      practical purposes, PCILynx controllers are not actually supported in the old stack either,
      and there was never any PCILynx owner who cared. (Instead, they use this hardware as a bus
      analyzer with the stand-alone driver nosy rather than as a controller with the old ieee1394
      pcilynx driver.)

Historical note: VIA VT6306 did not work properly for DV capture with gstreamer's ``dv1394src``
plugin or with the ``DV4Linux`` tool
(`report <http://thread.gmane.org/gmane.linux.kernel.firewire.user/4101>`_). This should be fixed
since kernel 3.10. VT6306 are shown by lspci as rev 46; higher chip revisions denote VT6307 or
VT6308 which were apparently not affected by this problem.

Installation of old drivers as fallback if the new ones are insufficient
------------------------------------------------------------------------

As a migration aid, it is possible until kernel 2.6.36 inclusive to build and install the new _and_
the old drivers together. However, care needs to be taken to keep in control which drivers are
loaded. I.e. create proper blacklist entries in ``/etc/modprobe.conf`` as explained below to avoid
auto-loading of the wrong drivers. ``Libraw1394`` v2 falls back to access the old drivers if the
new ones are not loaded/ not bound to hardware, transparently to ``libraw1394`` based libraries and
applications.

If you are having trouble with the new drivers, do not hesitate to get in touch via the
Mailinglists.

Why the migration?
==================

The code base of the new stack is smaller, cleaner, and closer follows modern Linux kernel
programming paradigms. There are already some features in the new stack which the old one lacks,
notably bus manager capability and so-called gap count optimization. The latter provides a
noticeable speedup of SBP-2 and other asynchronous protocols. Instead of 3 or 5 userspace
interfaces in the old stack, there is a single universal interface in the new stack. Furthermore,
there are some fundamental bugs and security considerations with the old stack which to a large
part motivated the rewrite of the driver stack.

Module auto-loading
===================

How to get auto-loading
-----------------------

The drivers ``firewire-ohci`` and ``firewire-sbp2`` contain the module aliases
``pci:v*d*sv*sd*bc0Csc00i10*`` and ``ieee1394:ven*mo*sp0000609Ever00010483*`` which should suffice
with hotplug scripts and recent coldplug scripts to automatically load these two drivers when
respective hardware is detected. The dependence of both drivers on ``firewire-core`` is of course
recognized by modprobe.

If the kernel was configured without ``ohci1394`` and ``sbp2`` as modules, then ``firewire-core`` and
``firewire-sbp2`` also contain the module aliases ``ohci1394`` and ``sbp2`` respectively. That is,
``modprobe ohci1394``" will load ``firewire-ohci`` (and ``firewire-core``) instead of ``ohci1394``
if ``ohci1394`` was excluded from the kernel build.

How to suppress auto-loading
----------------------------

To avoid confusion, it is recommended that either the old or the new driver stack is built, but not
both together. It is nevertheless possible to build and install both stacks together. If you chose
to do so, you may want to add lines like

::

    # blacklist ``firewire-ohci``
    # blacklist ``firewire-sbp2``
    # blacklist firewire-net
    
    blacklist ``ohci1394``
    blacklist sbp2
    blacklist eth1394
    blacklist dv1394
    blacklist raw1394
    blacklist video1394

to ``/etc/modprobe.d/file_of_your_choice`` or ``/etc/modprobe.conf`` to suppress auto-loading of
one of the stacks.

Extremely old modutils which do not support the blacklist keyword can be instructed by
configuration entries like ``install _module_ /bin/true`` to suppress loading of a particular
_module_.

Module blacklisting alone is of course insufficient if the drivers are statically linked into the
kernel. But less obvious, it is also insufficient if the set of drivers that you don't want are
already loaded by an initrd (initial RAM disk) during system boot. For example, the stock initrd of
Ubuntu 10.4 binds ``ohci1394`` to FireWire controllers. A remedy is to
`rebuild the initrd <https://bugs.launchpad.net/ubuntu/+source/kino/+bug/6290/comments/78>`_
with a reduced
`selection of modules <http://marc.info/?l=linux1394-user&m=130289059932203>`_.

Character device files, block device files
==========================================

Basic operation
---------------

Out of the box, udevd and udev scripts automatically create and remove

* ``/dev/fw*`` devices exposed by ``firewire-core`` (for use by libraries like ``libraw1394`` and
  ``libdc1394``, provided the libraries are updated)
* ``/dev/{sd,sg,sr,st}*`` devices exposed by SCSI command set drivers (sd_mod, sg, sr_mod, st) with
  ``firewire-sbp2`` underneath.

Permissions and ownership for /dev/fw*
---------------------------------------

Without any configuration file, the device files will be only accessible to root. This is fine and
intended for SCSI block device files. But it is usually desirable to access the ``/dev/fw*``
character device files as non-root user. The example udev rules shown below allow any user in group
``video`` to access ``/dev/fw*`` with programs such as ``dvgrab``, ``kino``, or ``coriander``.

By evaluating information in ``firewire-core``'s sysfs files, it is possible to automatically adapt
file permissions and ownership of ``/def/fw*`` files according to device types

* Files pertaining to local nodes (the controllers) and SBP-2 nodes (storage devices, scanners
  etc.) should get restrictive file permissions and ownership. Actually, the default should be
  restrictive on most systems.

* Files pertaining to audio and video devices (AV/C or IIDC compliant nodes) can get liberal
  permissions so that they are accessible to application programs without root privilege.

* Old ``libraw1394`` versions and some special-purpose ``libraw1394`` clients (e.g. ``gscanbus``)
  currently require also access to the local node. Its permissions need to be liberal in those
  cases too.  To the author's knowledge, this does not have any actual security impact on systems
  on which user access to AV/C and IIDC devices is already allowed, but distributors typically
  won't want to open this up per default.

The following rules implement this device type dependent control over permissions, using the
example ``video`` group. These rules work with Linux kernel 2.6.31 and later. These rules have been
merged into ``/lib/udev/rules.d/50-udev-default.rules`` of udev v144.

``/etc/udev/rules.d/example-firewire.rules`` ::

  # IIDC devices: industrial cameras and some webcams
  SUBSYSTEM=="firewire", ATTR{units}=="*0x00a02d:0x00010*", GROUP="video"
  
  # AV/C devices: camcorders, set-top boxes, TV sets, various audio devices, and more
  SUBSYSTEM=="firewire", ATTR{units}=="*0x00a02d:0x010001*", GROUP="video"

In udev v161, two more match patterns were added: ``*0x00b09d:0x00010*`` for IIDC cameras from
Point Grey and ``*0x00a02d:0x014001*`` for AV/C devices with vendor-unique command set.

Unfortunately, FireWire audio devices often adhere to vendor-specific protocols. Furthermore, an
``audio`` group assignment may be preferred over ``video``. Therefore, a special ruleset for the
FFADO FireWire audio library is provided by recent versions of ``libffado``
(`60-ffado.rules <http://subversion.ffado.org/browser/trunk/libffado/libffado/60-ffado.rules>`_).

If your application needs access to all nodes, simply use:

::

  SUBSYSTEM=="firewire", GROUP="video"

There are also schemes which are based on access control lists instead of UNIX file permissions.
For example, the Fedora Linux distribution currently contains a mechanism to add read and write
permission for the locally logged in user to the ACLs of ``fw*`` files of some FireWire device
types.  To this end, recent udev releases have firewire subsystem rules in the file
``70-acl.rules`` file.

Symlinks to SCSI block device files
===================================

Reasonably recent udev scripts create symbolic links in ``/dev/disk/by-id/*``, pointing to the
block devices of FireWire harddisks. These links are convenient for example to use them in static
fstab entries: Their names are always the same because they are based on persistent and unique
device identifiers, while the actual device files have arbitrary names that change all the time
when disks are plugged in and out.

The names of the by-id links look per default a little bit different with ``firewire-sbp2`` compared
to sbp2. To make them look exactly the same, add the following to
``/etc/modprobe.d/file_of_your_choice or /etc/modprobe.conf``:

::

  options ``sbp2`` long_ieee1394_id=y

This option has been added to ``sbp2`` in Linux 2.6.22.

You only need this if you plan to switch between ``sbp2`` and ``firewire-sbp2`` and if you are using the
``/dev/disk/by-id`` symlinks.

Hald support
============

Nothing special is needed; hotplug and hot-removal of ``firewire-sbp2`` driven disks is exposed on
desktops just fine.

Initrd
======

Scripts which generate initrd (an initial RAM disk used during boot) may need to be updated to deal
with the new kernel module names ``firewire-core``, ``firewire-ohci``, ``firewire-sbp2``. Scripts within initrd
may already work with the new drivers.

Libraries
=========

libraw1394
----------

_At the time of this writing (July 2012), ``libraw1394`` v2.1.0 is the current release and also the
recommended stable release for use with the new firewire drivers._

Compatibility with the new drivers is available since ``libraw1394`` v2.0.0, released in July 2008.
This version is able to transparently switch between old and new stack, depending on which drivers
you have loaded. Bug fixes related to usage with the new kernel drivers followed in further v2.0.x
sub-releases.

Either get the latest ``libraw1394`` 2.x release, or build ``libraw1394`` from a fresh git checkout:

::

    $ git clone git://git.kernel.org/pub/scm/libs/ieee1394/``libraw1394``.git
    $ cd ``libraw1394``/
    $ autoreconf -fi
    $ ./configure
    $ make
    $ sudo make install

The Juju backend of ``libraw1394`` requires the inotify kernel interface. If you have got a very old
libc which does not contain support for inotify, you can have a look at the following patches for
``libraw1394``: `patch 1 <http://marc.info/?l=linux1394-devel&m=120058086516766>`_,
`patch 2 <http://marc.info/?l=linux1394-devel&m=120058086516770>`_. They don't apply to recent
versions of ``libraw1394`` anymore though.

Libraw1394's Juju backend furthermore expects that the kernel was configured and built with the
following options:

::

    CONFIG_INOTIFY_USER=y
    CONFIG_EPOLL=y

The inotify option is located in the "File systems" section of the Kconfig dialogs.
``CONFIG_EPOLL`` is already enabled in usual kernel configurations and not even visible in the
Kconfig dialogs. But if the kernel was configured for embedded systems it may be necessary to
switch ``CONFIG_EPOLL`` on explicitly; it is then found in the ``General setup`` section of the
kernel configuration.

libdc1394
---------

.. note: At the time of this writing (May 2012), ``libdc1394`` v2.2.0 is the current release and
   also the recommended stable release for use with the new firewire drivers.

There is compatibility with ``libdc1394`` v2, released in January 2008. Some deployed ``libdc1394``
applications still use the older ``libdc1394`` v1 though which has no support for the Juju drivers.
Since v2.1.0, ``libdc1394`` transparently supports the old and the new drivers, though with
different extent of functionality. Since ``libdc1394`` v2.1.2 from June 2009 and kernel 2.6.30,
``libdc1394`` 's Juju backend fully supports multiple cameras on the same bus, precise packet
timestamps/ framerate measurement, and isochronous resource clean-up even at unclean program
termination — i.e. offers same or better functionality with the new drivers compared to the old
ones.

You can check ``libdc1394`` v2 out from the project's repository and build it with support for both
the new and the old driver stack by the following steps:

::

    $ git clone git://libdc1394.git.sourceforge.net/gitroot/libdc1394/libdc1394
    $ cd libdc1394/libdc1394/
    $ autoreconf -fi
    $ ./configure
    $ make
    $ sudo make install

(In ``libdc1394`` until v2.0.3 inclusive, a configure switch called "--with-juju-dir" was necessary
to support the new driver ABI, e.g. ``./configure --with-juju-dir=/usr/src/linux/include``. But this
also disabled support for the old ieee1394 stack.)

``Libdc1394`` is able to work with the new drivers even if you have only a ``libraw1394`` v1
installed alongside ``libdc1394``. This is because ``libdc1394`` requires ``libraw1394`` only in
order to use the old drivers while it accesses the new drivers without assistance of
``libraw1394``.

No special kernel headers package is required to build ``libdc1394``.

FFmpeg: libavformat
-------------------

Alas the ``dv1394`` module of `FFmpeg <http://ffmpeg.mplayerhq.hu/>`_ s libavformat has not been
ported to ``firewire-core`` yet. This particularly affects players like MPlayer and Xine. However,
you can use the capture utility ``dvgrab`` to provide input to players:

::

    $ dvgrab - | xine stdin:/

pwlib: AV/C and IIDC plugins
----------------------------

The `AV/C video input module <http://opalvoip.svn.sourceforge.net/viewvc/opalvoip/ptlib/trunk/plugins/vidinput_avc/vidinput_avc.cxx>`_
of ``pwlib`` a.k.a. ``ptlib``, used in `Opal <http://www.opalvoip.org/>`_,
`Ekiga <http://www.ekiga.org/>`_ etc. uses the old ``raw1394_start_iso_rcv`` API which is no longer
implemented in the kernel since Release Notes/2.6.2x.

The `IIDC video input module <http://opalvoip.svn.sourceforge.net/viewvc/opalvoip/ptlib/trunk/plugins/vidinput_dc/video4dc1394.cxx>`_
(a.k.a. DC module) of ``pwlib``/``ptlib`` uses ``libdc1394`` v1 to which the new drivers are not
compatible.

Best would be if we had a V4L2 interface to FireWire cameras; then these applications would work
out of the box without special 1394 backends and libraries.
