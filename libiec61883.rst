===========
libiec61883
===========

2023/02/18 Takashi Sakamoto

.. contents:: Contents
   :depth: 2
   :local:
   :backlinks: top

Description
===========

This library provides a higher level API for streaming DV, MPEG-2, and audio and music data over
Linux IEEE 1394, defined in the series of IEC 61883.

Current Status
==============

* Inactive development
* Just maintained

Repository location
===================

* `<https://git.kernel.org/pub/scm/libs/ieee1394/libiec61883.git/>`_

Reference manual
================

* Not available yet.

Release notes
=============

1.2.0 (15 January 2009)
-----------------------

  * Fixed PID handling in MPEG2-TS.
  * Added ``iec61883_cip_resync()``.
  * Silence some warnings in CMP that may needlessly alarm users.
  * Improved validation of DV packets in ``dv_fb``.
  * bugfixes

1.1.0 (24 September 2006)
-------------------------

  * This is a maintenance release that contains some minor fixes and cleanups, but it also requires
    ``libraw1394`` 1.2.1 to prevent aborts and halts in isochronous transmission.

1.0.0 (8 April 2005)
--------------------

  * After many months of hard work, the first release of ``libiec61883`` is now available. This
    library provides third generation media reception and transmission for DV, MPEG2-TS, and AMDTP
    (audio and music) using only ``raw1394`` and not the complicated setup and maintenance of other
    kernel modules and their ``/dev`` nodes. This is not just an early development release. It is
    already quite capable and robust. Already, ``MythTV`` 0.17 supports MPEG2-TS, ``FreeBob`` is
    using AMDTP, and a soon-to-be-released ``dvgrab`` 2.0 is being used in a heavy, 24/7 production
    environment. We encourage all developers and applications to migrate to this library as support
    for ``dv1394``, ``amdtp``, and ``cmp`` modules are being phased out as well as DV and MPEG2-TS
    applications of ``video1394``.

  * NOTE: ``libiec61883`` requires ``libraw1394`` 1.2.0.
