==============
Specifications
==============

2023/02/17 Takashi Sakamoto

.. contents:: Contents
   :depth: 2
   :local:
   :backlinks: top

1394 Trade Association (1394TA)
===============================

1394TA was the official industry consortium behind 1394 technology, and already dissolved. They
housed specifications for AV/C and other protocols as well as hardware related specifications.

IEEE 1394
=========

IEEE Std 1394-2008
------------------

* `<https://ieeexplore.ieee.org/document/4659233>`_

From the 1394 TA's press release: "The 1394-2008 High Performance Serial Bus Standard updates and
revises all prior 1394 standards dating back to the original 1394-1995 version, and including
1394a, 1394b, 1394c, enhanced UTP, and the 1394 beta plus PHY-Link interface. It also incorporates
the complete specifications for S1600 (1.6 Gigabit/second bandwidth) and for S3200, which provides
3.2 Gigabit/second speeds."

IEEE Std 1394.1-2004
--------------------

* `<https://ieeexplore.ieee.org/document/1490130>`_

IEEE Standard for High Performance Serial Bus Bridges. Defines bridging devices which connect
separate FireWire buses. No implementations are known. The Linux drivers don't have explicit
support for 1394.1 yet but should at least be prepared to deal with bus IDs other than the local
bus.

IEEE Std 1394-1995
------------------

* `<https://ieeexplore.ieee.org/document/526693>`_

IEEE standard for a high performance serial bus

IEEE Std 1394a-2000
-------------------

* `<https://ieeexplore.ieee.org/document/853984>`_

IEEE Standard for a high Performance Serial Bus - Amendment 1. A lot of the IEEE 1394a updates are
relevant to programmers.

IEEE Std 1394b-2002
-------------------

* `<https://ieeexplore.ieee.org/document/1146719>`_

IEEE Standard for a High-Performance Serial Bus - Amendment 2. Defines S100B...S1600B transmission
modes over copper and optical fibre. Only very few details of this huge update to IEEE 1394 are
relevant to programmers.

IEEE Std 1394c-2006
-------------------

* `<https://ieeexplore.ieee.org/document/4231515>`_

IEEE Standard for a High-Performance Serial Bus - Amendment 3. Defines S800T, an 800 Mbit/s
transmission mode over gigabit Ethernet copper. Probably not (yet) relevant to programmers.

IEEE 1212
=========

IEEE Std 1212-2001
------------------

* `<https://ieeexplore.ieee.org/document/1032653>`_

IEEE standard for a control and status registers (CSR) architecture for microcomputer buses.
IEEE 1394 is an implementation of IEEE 1212 (the most important one, and the only one besides
SCI, Scalable Coherent Interface). IEEE 1212 especially defines the format and generic contents
of the configuration ROM.

ISO/IEC 13213:1994
------------------

* `<https://www.iso.org/standard/21416.html>`_

This was revised and hence superseded by IEEE 1212-2001.

1394 Open Host Controller Interface Specification
=================================================

OHCI Release 1.2
----------------

Among else, this was apparently supposed to update OHCI for 1394b. But this spec was never
finished. Currently available 1394b host adapters implement OHCI 1.1 or a superset of it.

OHCI Release 1.1 (January 6, 2000)
----------------------------------

* `<https://download.microsoft.com/download/1/6/1/161ba512-40e2-4cc9-843a-923143f3456c/ohci_11.pdf>`_

Look here for the most in-depth technical information about IEEE 1394 technology, at least as it
applies to OHCI. This spec is gratis, whereas the IEEE specifications have to be purchased.
OHCI 1.1 especially adds dual-buffer reception.

OHCI Release 1.00 (October 20, 1997)
------------------------------------

Some controllers, e.g. VIA VT6306 which is still in production, only support OHCI 1.0 instead of
the more current OHCI 1.1.

PCILynx datasheets
==================

* SCPA020A — PCILynx 1394 to PCI Bus Interface TSB12LV21BPGF Functional Specification
* SLLA023 — Initialization and Asynchronous Programming of the PCILynx TSB12LV21A 1394 Device

PCILynx is an older 1394 PCI controller which is not OHCI compliant.

The Serial Bus Protocol
=======================

SBP-3
-----

* `<http://webstore.ansi.org/RecordDetail.aspx?sku=ANSI+INCITS+375-2004>`_
* `<http://www.t10.org/drafts.htm#sbp3>`_

Last publicly available draft:
`<http://web.archive.org/web/20040830205905/http://www.t10.org/ftp/t10/drafts/sbp3/sbp3r05.pdf>`_

SBP-3 is a superset of SBP-2. It adds isochronous support to Serial Bus Protocol, particularly for
A/V applications. It also adds support for remote buses behind 1394.1 bridges. Current devices that
show themselves as SBP-3 compliant do not appear to implement any of SBP-3's additions relative to
SBP-2 though.

Other SCSI specifications and working drafts
--------------------------------------------

* `<http://www.t10.org/scsi-3.htm>`_
* `<http://www.t10.org/drafts.htm>`_
* `<http://web.archive.org/web/20070808174746/http://www.t10.org/ftp/t10/drafts/>`_

SBP-2
-----

* `<http://webstore.ansi.org/RecordDetail.aspx?sku=ANSI+INCITS+325-1998+(R2008)>`_
* `<http://www.t10.org/drafts.htm#sbp2>`_

Last publicly available draft: `<http://web.archive.org/web/20050118090648/www.t10.org/ftp/t10/drafts/sbp2/sbp2r04.pdf>`_

Serial Bus Protocol 2 is a SCSI transport protocol but can also be used to transport commands of
non-SCSI command sets. All of the FireWire storage devices (harddisks, CD/DVD-ROM/R/W, card
readers, scanners, ...) implement SBP-2 and typically one or another SCSI command set such as RBC
or MMC.

SBP
---

This is obsolete and practically irrelevant.

Instrumentation and Industrial Control Digital Camera (IIDC) Specification
==========================================================================

IIDC2
-----

IIDC2 is a joint specification of JIIA and 1394TA for machine vision products in industrial,
commercial and consumer markets. IIDC2 is based on IIDC but not register-compatible. IIDC or
IIDC2 or both together may be implemented by a camera. Goals of IIDC2 are simpler implementation,
extensibility, and applicability not only to IEEE 1394 but also to CoaXPress, CameraLink, or USB.

IIDC
----

This specification covers uncompressed video and camera control (focus, format, pan, zoom, white
balance, etc.). Also known as DCAM.

IEC 61883
=========

* Consumer audio/video equipment - Digital interface

  * Part 1: General
  * Part 2: SD-DVCR data transmission
  * Part 3: HD-DVCR data transmission
  * Part 4: MPEG2-TS data transmission
  * Part 5: SDL-DVCR data transmission (TA4)
  * Part 6: Audio and music data transmission protocol
  * Part 7: Transmission of ITU-R BO.1294 System B

IP over 1394
============

RFC 2734
--------

* `<http://www.ietf.org/rfc/rfc2734.txt>`_
* `<http://www.apps.ietf.org/rfc/rfc2734.html>`_

IPv4 over IEEE 1394

RFC 2855
--------

* `<http://www.ietf.org/rfc/rfc2855.txt>`_
* `<http://www.apps.ietf.org/rfc/rfc2855.html>`_

DHCP for IEEE 1394

RFC 3146
--------
* `<http://www.ietf.org/rfc/rfc3146.txt>`_
* `<http://www.apps.ietf.org/rfc/rfc3146.html>`_

Transmission of IPv6 Packets over IEEE 1394 Networks
