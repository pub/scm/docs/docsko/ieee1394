==========================
Database of PHY device IDs
==========================

2024/04/02 Takashi Sakamoto

.. contents:: Contents
   :depth: 2
   :local:
   :backlinks: top

Overview
========

A PHY connects the controller to the actual ports and manages the low-level electrical details.

Most motherboard and add-on card controllers have the PHY integrated in the same chip; most
embedded devices have a separate PHY chip, often from TI.

For example, for a PCI FireWire controller, the chip model is identified by its PCI ID on the PCI
side (shown by ``lspci``, and by its !FireWire ID on the FireWire side (shown by ``lsfirewirephy``
command in
`linux-firewire-utils <https://git.kernel.org/pub/scm/utils/ieee1394/linux-firewire-utils.git/>`_)

::

          PCI(e) bus  +------------++-----+   FireWire bus
    ... --+----+------| controller || PHY |----+-----+----- ...
          |    |      +------------++-----+    |     |
         ...  ...                             ...   ...

How to add new entries
======================

Run ``lsfirewirephy`` in
`linux-firewire-utils <https://git.kernel.org/pub/scm/utils/ieee1394/linux-firewire-utils.git/>`_
to get a list of PHY vendor/device IDs.

Please post the numbers and the chip name to the
`linux1394-devel <http://lists.sourceforge.net/mailman/listinfo/linux1394-devel>`_.

PHY device IDs
==============

* 00000e: `Fujitsu <http://www.fujitsu.com/>`_

  * 00000e:086613 MB86613

* 00004c: `NEC <http://www.nec.co.jp/>`_

  * 00004c:000201 PD728xx “OrangeLink”
  * 00004c:050160 PD72873 “Firewarden”

* 00053d: `Broadcom <http://www.broadcom.com/>`_ (formerly known as `Agere <http://agere.com/>`_,
  `LSI <http://www.lsi.com/>`_, `Avago <http://www.avagotech.com/>`_)

  * 00053d:0533xx FW533E “TrueFire”
  * 00053d:0643xx FW643 “TrueFire”
  * 00053d:0643xx FW643E “TrueFire”
  * 00053d:0843xx FW843

* 000cc2: ControlNet India (chip design for O2Micro)

  * 000cc2:401104 OZxxx

* 001163: `System S.p.A. <http://www.system-group.it/>`_ (actually from
  `VIA <http://www.via.com.tw/>`_, but with a wrong ID)

  * 001163:306001 `VT6306 “Fire” <http://www.via.com.tw/en/products/peripherals/1394/vt6306l/>`_
  * 001163:306001 `VT6307 “Fire IIM” <http://www.via.com.tw/en/products/peripherals/1394/vt6307ls/>`_
  * 001163:306001 `VT6308 “Fire IIM” <http://www.via.com.tw/en/products/peripherals/1394/vt6308ps/>`_
  * 001163:306001 `VT6312 “Fire” <http://www.via.com.tw/en/products/peripherals/1394/vt6312/>`_
  * 001163:306001 `VT6315 “Fire” <http://www.via.com.tw/en/products/peripherals/1394/vt6315/>`_
  * 001163:306001 `VT6320 “Fire” <http://www.via.com.tw/en/products/peripherals/1394/vt6320/>`_
  * 001163:306001 `VT6325 “Fire” <http://www.via.com.tw/en/products/peripherals/1394/vt6325/>`_
  * 001163:306001 `VT6330 “Fire” <http://www.via.com.tw/en/products/peripherals/1394/vt6330/>`_

* 001454: `Microchip <http://www.microchip.com/>`_ (formerly known as
  `Symwave <http://symwave.com/>`_)

  * 001454:003181 SW3080 “FirePHY”

* 001b8c: `JMicron <http://www.jmicron.com/>`_

  * 001b8c:038100 `JMB381 <http://www.jmicron.com/JMB38X.html>`_

* 004063: `VIA <http://www.via.com.tw/>`_

  * (see above)

* 00601d: `Broadcom <http://www.broadcom.com/>`_ (formerly known as
  `Lucent <http://www.lucent.com>`_, `LSI <http://www.lsi.com/>`_,
  `Avago <http://www.avagotech.com/>`_)

  * 00601d:0322xx FW322 “TrueFire”
  * 00601d:0323xx FW323 “TrueFire”
  * 00601d:0802xx FW802 “TrueFire”

* 006037: `NXP <http://www.nxp.com/>`_ (formerly known as `Philips <http://www.philips.com/>`_)

  * 006037:412801 PDI1394P25
  * 006037:422001 PDI1394P23
  * 006037:4239x0 PDI1394P24
  * 006037:431000 PDI1394P21
  * 006037:431100 PDI1394P22

* 00c02d: `Fujifilm <http://www.fujifilm.com/>`_

  * 00c02d:303562 MD8405B
  * 00c02d:303565 MD8405E

* 080028: `Texas Instruments <http://www.ti.com/>`_

  * 080028:42308a `TSB41LV02A <http://www.ti.com/product/tsb41lv02a>`_
  * 080028:424296 `TSB41AB1 <http://www.ti.com/product/tsb41ab1>`_
  * 080028:424296 `TSB41AB2 <http://www.ti.com/product/tsb41ab2>`_
  * 080028:424499 `TSB43AB22 “iOHCI-Lynx” <http://www.ti.com/product/tsb43ab22>`_
  * 080028:424499 `TSB43AB22A “iOHCI-Lynx” <http://www.ti.com/product/tsb43ab22a>`_
  * 080028:424729 `XIO2200A <http://www.ti.com/product/xio2200a>`_
  * 080028:434195 `TSB41AB3 <http://www.ti.com/product/tsb41ab3>`_
  * 080028:434502 `TSB43AB23 <http://www.ti.com/product/tsb43ab23>`_
  * 080028:434615 `TSB43CB43A “iceLynx Micro” <http://www.ti.com/product/tsb43cb43a>`_
  * 080028:46318a `TSB41LV06A <http://www.ti.com/product/tsb41lv06a>`_
  * 080028:831304 `TSB81BA3 <http://www.ti.com/product/tsb81ba3>`_
  * 080028:831304 TSB81BA3A
  * 080028:831306 `TSB81BA3D <http://www.ti.com/product/tsb81ba3d>`_
  * 080028:831307 `TSB81BA3E <http://www.ti.com/product/tsb81ba3e>`_
  * 080028:831307 `XIO2213A “Cheetah Express” <http://www.ti.com/product/xio2213a>`_
  * 080028:831307 `XIO2213B “Cheetah Express” <http://www.ti.com/product/xio2213b>`_
  * 080028:833003 `TSB41BA3B <http://www.ti.com/product/tsb41ba3b>`_
  * 080028:833005 `TSB41BA3D <http://www.ti.com/product/tsb41ba3d>`_

* 10005a: `IBM <http://www.ibm.com/>`_

  * 10005a:21860x IBM21S860
  * 10005a:21861x IBM21S861
  * 10005a:21862x IBM21S862

Original document
=================

This page is copied from `<https://github.com/cladisch/linux-firewire-utils/wiki/PhyIDs>`_ in the
original repository for ``linux-firewire-utils``.
